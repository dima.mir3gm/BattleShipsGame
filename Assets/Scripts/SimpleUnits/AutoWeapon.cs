﻿using UnityEngine;
using System.Collections;
using System;

public class AutoWeapon : SimpleWeapon, IOnTargetFounded
{

    [SerializeField] private float rotation_speed = 50;
    private float current_target_range;
    private GameObject current_target;
    private float end_angle;

    private RotateGunTypes rotate_action;

    void Start()
    {
        Unit unit = null;
        Transform t = transform;
        while (unit == null)
        {
            unit = t.GetComponent<Unit>();
            t = t.parent;
        }
       unit.GetComponentInChildren<Radar>().AddTargetFinderListener(this);
    }

    public override void Attack()
    {
        base.Attack();
    }

    public void OnTargetFounded(GameObject target, float dist)
    {
        
        if(current_target_range > dist || current_target_range == 0)
        {
            current_target = target;
        }
    }

    protected override void Update()
    {
        base.Update();
        if (current_target != null)
        {
            CalcTargetAngle();
            RotateGun();
        }
        Attack();
    }

    private void CalcTargetAngle()
    {
        end_angle = Vector3.Angle(transform.position, current_target.transform.position);

        float dist = Vector3.Distance(transform.position, current_target.transform.position);
        //float cat1 = transform.position.x - current_target.transform.position.x;
        //float cat2 = transform.position.y - current_target.transform.position.y;

        float x_me = transform.position.x;
        float y_me = transform.position.y;
        float x_tg = current_target.transform.position.x;
        float y_tg = current_target.transform.position.y;

        if(x_me <= x_tg && y_me <= y_tg)
        {
            end_angle = 360 - Mathf.Asin((x_tg - x_me) / dist) * Mathf.Rad2Deg;
        }
        else if(x_me > x_tg && y_me <= y_tg)
        {
            end_angle = Mathf.Asin((x_me - x_tg) / dist) * Mathf.Rad2Deg;

        }
        else if (x_me < x_tg && y_me > y_tg)
        {
            end_angle = 180 + Mathf.Asin((x_tg - x_me) / dist) * Mathf.Rad2Deg;
        }
        else if(x_me > x_tg && y_me > y_tg)
        {
            end_angle =  180 - Mathf.Asin((x_me - x_tg) / dist) * Mathf.Rad2Deg;
        }

        //Debug.LogError("DIST = " + dist + " X_ME = "  + x_me + " X_TARG  = " + x_tg);
    }

    private void RotateGun()
    {
        float curr_angel = transform.eulerAngles.z;
        if (curr_angel > end_angle - 10 && curr_angel < end_angle + 10)
        {
            curr_angel = end_angle;
            transform.eulerAngles = new Vector3(0, 0, end_angle);
            rotate_action = RotateGunTypes.None;
        }
        else
        {
            if (curr_angel > end_angle + 180)
            {
                rotate_action = RotateGunTypes.Right;
            }
            else if (curr_angel > end_angle)
            {
                rotate_action = RotateGunTypes.Left;
            }
            else if (curr_angel + 180 < end_angle)
            {
                rotate_action = RotateGunTypes.Left;
            }
            else if (curr_angel < end_angle)
            {
                rotate_action = RotateGunTypes.Right;
            }
        }

        //Debug.LogError(rotate_action + " MY_ANGLE = " + curr_angel + " TARG_ANGLE = " + end_angle);
        DoAction();
    }

    private void DoAction()
    {
        switch (rotate_action)
        {
            case RotateGunTypes.Right:
                {
                    transform.eulerAngles += new Vector3(0, 0, rotation_speed * Time.deltaTime);
                    break;
                }

            case RotateGunTypes.Left:
                {
                    transform.eulerAngles += new Vector3(0, 0, -rotation_speed * Time.deltaTime);
                    break;
                }

            case RotateGunTypes.None:
                {

                    break;
                }

            case RotateGunTypes.Cancel:
                {
                    break;
                }
        }
        
    }

    public enum RotateGunTypes
    {
        Right,
        Left,
        Cancel,
        None
    }
}
