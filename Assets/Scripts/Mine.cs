﻿using UnityEngine;
using System.Collections;

public class Mine : MonoBehaviour {

    [SerializeField] private float destruction_delay = 0.1f;
    private float mine_life;
    private MinePool pool;
    private float damage;
    private Unit parent;

    private float currentTime;

    public float MineLife
    {
        get { return mine_life; }
        set { mine_life = value; }
    }

    public MinePool Pool
    {
        get { return pool; }
        set { pool = value; }
    }

    public float Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    public Unit Parent
    {
        set { parent = value; }
        get { return parent; }
    }

    public void Destroy()
    {
        currentTime = mine_life - destruction_delay;
    }

    void Update()
    {
        if (pool == null)
            return;
        if (currentTime < mine_life)
        {
            currentTime += Time.deltaTime;
        }
        else
        {
            currentTime = 0;
            pool.DestroyMine(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        string tag = col.tag;
        if (col.gameObject == parent.gameObject)
        {
        }
        else if (col.GetComponent<Unit>() != null)
        {
            col.GetComponent<Unit>().Hit(parent, damage);
            Destroy();
        }
        /*
        else if (tag.Equals("Bullet") && parent != col.GetComponent<Bullet>().Parent)
        {
            col.GetComponent<Bullet>().Destroy();
            Destroy();
        }
        */
    }
}
