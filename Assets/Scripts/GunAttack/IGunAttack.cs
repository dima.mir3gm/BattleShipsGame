﻿using UnityEngine;
using System.Collections;

public interface IGunAttack
{

    void Fire(float damage, float range);


}
