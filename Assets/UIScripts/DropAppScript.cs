﻿using UnityEngine;
using System.Collections;

public class DropAppScript : MonoBehaviour {

    [SerializeField] private float delay;
    private float timer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (timer < delay)
            timer += Time.deltaTime;
        else
            Application.Quit();
	}
}
