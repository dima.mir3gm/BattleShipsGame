﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainPanelUIController : MonoBehaviour {

    [SerializeField] private AnalyticsController analytics;
    [SerializeField] private string title = "MainPanel";
    [SerializeField] private string action = "Confirmed";
    [SerializeField] private BarLoader loader;
    [SerializeField] private Text loginField;

	void Start ()
	{
	    if (analytics == null) analytics = FindObjectOfType<AnalyticsController>();
	}

    public void onConfimClick()
    {
        SaveUsername save = analytics.GetComponent<SaveUsername>();
        analytics.LogEvent(title, action);
        if (loginField != null)
        {
            save.Username = loginField.text;
        }

        if (save.GameCounter == 0)
        {
            save.GameCounter++;
            GetComponentInParent<PanelsController>().OffPanel(0);
            GetComponentInParent<PanelsController>().OnPanel(1);

            if (loader != null)
                loader.startBar();
        }
        else
        {
            SceneManager.LoadScene("game");
        }
        

        //GetComponentInParent<PanelsController>().hideAllErrorPanels();
    }
}
