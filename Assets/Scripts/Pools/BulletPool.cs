﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletPool : MonoBehaviour
{


    [SerializeField] private GameObject bullet;
    [SerializeField] private int pool_Size = 1000;

    private int bullet_counter;
    private int used_bullet;

    private List<GameObject> bullet_pool;
    private List<GameObject> free_bullets;
    private List<GameObject> using_bullets;

    void Awake()
    {
        bullet_pool = new List<GameObject>(pool_Size);
        free_bullets = new List<GameObject>(pool_Size);
        using_bullets = new List<GameObject>(pool_Size);
        CreateBullets(pool_Size);
    }

    public GameObject InstantiateBullet()
    {
        if (free_bullets.Count == 0)
        {
            CreateBullets(pool_Size/10);
        }
        GameObject bullet1 = free_bullets[0];
        bullet1.transform.position = Vector3.zero;
        bullet1.SetActive(true);
        free_bullets.Remove(bullet1);
        using_bullets.Add(bullet1);

        used_bullet = using_bullets.Count;
        return bullet1;
    }

    private void CreateBullets(int count)
    {
        bullet_counter += count;
        for (int i = 0; i < count; i++)
        {
            GameObject bullet1 = Instantiate(bullet) as GameObject;
            bullet1.SetActive(false);
            bullet1.transform.position = transform.position;
            bullet1.transform.parent = transform;
            bullet1.name = bullet1.GetInstanceID().ToString();
            bullet1.GetComponent<Bullet>().Pool = this;

            bullet_pool.Add(bullet1);
            free_bullets.Add(bullet1);
        }
    }

    public void DestroyBullet(GameObject obj)
    {
        using_bullets.Remove(obj);
        free_bullets.Add(obj);
        obj.SetActive(false);
        obj.transform.position = transform.position;
        obj.transform.parent = transform;
        obj.transform.localScale = Vector3.one;
    }
}
