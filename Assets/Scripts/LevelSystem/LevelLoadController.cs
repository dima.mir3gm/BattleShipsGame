﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelLoadController : MonoBehaviour, OnStateCanBeUpdated
{

    [SerializeField] GameObject[] loads;
    [SerializeField] int count = 12;

    [SerializeField] private Color32 open_update;
    [SerializeField] private Color32 close_update;

    private float baseSize;
    private float baseHeigth;

	// Use this for initialization
	void Awake ()
	{
	    Rect rs = loads[0].GetComponent<RectTransform>().rect;
	    baseSize = 360 - 20;
	    baseHeigth = 20;

	}

    void Start()
    {
        GetComponent<LevelPanelListener>().LevelSystem.AddStateHandler(this);
    }

    public void ChangeLoad(int numb, int lvl)
    {
        RectTransform rt = loads[numb].GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(-360 + (baseSize * (lvl + 1))/count, -10);
    }


    public void SetActiveLoad(int numb, bool state)
    {
        Color tmp;
        if (state)
            tmp = open_update;
        else
            tmp = close_update;
        loads[numb].GetComponent<Image>().color = tmp;
    }

    public void OnStateCanBeUpdated(bool[] states)
    {
        for (int i = 0; i < states.Length; i++)
        {
            SetActiveLoad(i, states[i]);
        }
    }
}
