﻿using UnityEngine;
using System.Collections;
using JetBrains.Annotations;

public abstract class BonusLevelStructure
{

    public static readonly float[] HP_BONUS = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};

    public static readonly float[] HP_REGEN_BONUS = {0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300};

    public static readonly float[] CANON_RANGE_BONUS = { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };

    public static readonly float[] CANON_DAMAGE_BONUS = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};

    public static readonly float[] RELOAD_SPEED_BONUS = {0, 15, 30, 45, 60, 75, 90, 105, 120, 135, 150};

    public static readonly float[] MOVMENT_SPEED_BONUS = {0, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30};

    public static readonly float[] TURN_SPEED_BONUS = {0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50};

    public static readonly float[] RAM_DAMAGE_BONUS = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};

}
