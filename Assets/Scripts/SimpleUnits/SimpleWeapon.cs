﻿using UnityEngine;
using System.Collections;

public class SimpleWeapon : Weapon
{
    [SerializeField] private float animation_speed_coef = 0.95f;



    public override void Attack()
    {
        if (BeforeAttack())
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.PunchScale(new Vector3(0.2f,0.2f,0),animation_speed_coef * ReloadSpeed,0);

            }

            foreach (var gun in GetComponentsInChildren<IGunAttack>())
            {
                gun.Fire(Damage, Range);
            }

            AfterAttack();
        }
    }

    public override void SetPerfomance(float damage, float reload_speed, float range)
    {
        Damage = damage;
        ReloadSpeed = reload_speed;
        Range = range;
    }
}
