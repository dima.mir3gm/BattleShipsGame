﻿using UnityEngine;
using System.Collections;
using System;

public class MineDropper : MonoBehaviour, IGunAttack {

    [SerializeField] private float mineTime = 2;
    [SerializeField] private Vector3 mineLocalPosition = new Vector3(0, 15, 0);
    [SerializeField] private float mineMultipleLife = 5;
    [SerializeField] private float damageMultiple = 2;

    private MinePool pool;
    private Unit unit;

    private float mineTimer = 0;
    private float dmg = 1;
    private float range = 1;

    public void Fire(float damage, float range)
    {
        dmg = damage;
        this.range = range;
    }

    // Use this for initialization
    void Start ()
    {
        pool = GameObject.Find("MinePool").GetComponent<MinePool>();
        Transform t = transform;
        while (unit == null)
        {
            unit = t.GetComponent<Unit>();
            t = t.parent;
        }
    }
	
	// Update is called once per frame
	void Update () {
	    DropMine();
	}

    private void DropMine()
    {
        if(mineTime > mineTimer)
        {
            mineTimer += Time.deltaTime;
        }
        else
        {
            mineTimer = 0;
            GameObject mine = pool.InstantiateMine();
            mine.transform.position = transform.position;
            Mine mine1 = mine.GetComponent<Mine>();
            mine1.Damage = unit.CannonDamage;
            mine1.MineLife = unit.CannonRange * mineMultipleLife;
            mine1.Parent = unit;

        }
    }
}
