﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using AppodealAds.Unity.Api;

public class BarLoader : MonoBehaviour {

    [SerializeField] private float maxVal = 100;
    [SerializeField][Range(0.01f, 5f)] private float updateFrequancy = 1f;
    [SerializeField] private Image bar;
    [SerializeField] private Image barLoad;
    [SerializeField] private GameObject objectForCall;
    [SerializeField] private string method;
    [SerializeField] private AdController ad;
    [SerializeField] private AnalyticsController analytics;
    private float timer = 0;
    private float steep;
    private float max_size;
    private PanelsController controller;

    private bool isShowed = false;

    void Start()
    {
        if (analytics == null) analytics = GameObject.FindObjectOfType<AnalyticsController>();
        if (!ad) ad = FindObjectOfType<AdController>();
        isShowed = false;
        controller = GameObject.Find("Canvas").GetComponent<PanelsController>();
        max_size = barLoad.rectTransform.rect.width;
        steep = max_size / maxVal;
        Debug.Log("Size x = " + barLoad.rectTransform.rect.width);
    }

    void Update()
    {
        timer += Time.deltaTime;
        float newSize = timer * steep;
        if (newSize >= max_size)
        {
            timer = 0;
            isShowed = false;
            if (objectForCall != null)
            {
                //Debug.Log("SERVER BUSY!");
                Appodeal.hide(Appodeal.BANNER_TOP);
                //showVideo();
                //controller.showServerBusyPanel();
                objectForCall.SendMessage(method);
            }
        }
        else if(newSize >= max_size/2 && !isShowed)
        {
            isShowed = true;
            showVideo();
            
        }
        barLoad.rectTransform.sizeDelta = new Vector2(-max_size + timer *steep, 0);
    }

    public void startBar()
    {
        timer = 0;
        if (!ad) ad = FindObjectOfType<AdController>();
        if(ad) ad.ShowBar();
    }

    public void StopBar()
    {
        if(ad) ad.StopBar();
    }

    public void showVideo()
    {
        analytics.LogEvent("REKLAMA", "NA RUSSKOM RADIO");
        if (ad != null)
            ad.ShowVideo();
    }
    
}
