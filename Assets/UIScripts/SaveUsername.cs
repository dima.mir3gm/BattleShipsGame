﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SaveUsername : MonoBehaviour
{
    private static SaveUsername _instance;

    [SerializeField] private int gameCounter = 0;
    [SerializeField] private float updateTime = 1;
    private float current_time = 0;
    private bool standed;
    private float _expCoef = 1f;
    private Text _coefField;

    private string username;
    private bool _isGodMode;

    public int GameCounter
    {
        set { gameCounter = value; }
        get { return gameCounter; }
    }

    public bool IsGodMode { get { return _isGodMode; } set { _isGodMode = value; standed = false; } }

    public string Username
    {
        get { return username; }
        set
        {
            username = value;
            standed = false;
        }
    }

    public float ExpCoef
    {
        set
        {
            _expCoef = value;
            if (_coefField == null)
            {
                GameObject.Find("ExpCoefTextField").GetComponent<Text>().text = _expCoef + "X";
            }
        }
        get { return _expCoef; }
    }

    public void UpCoeficient()
    {
        ExpCoef += 0.2f;
    }

    public SaveUsername()
    {
        _instance = this;
    }

    private void Awake()
    {
        if (FindObjectsOfType<SaveUsername>().Length <= 1)
        {
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

	// Update is called once per frame
	void Update () {
        if(standed)
            return;

	    if (current_time < updateTime)
	    {
	        current_time += Time.deltaTime;
	    }
	    else
	    {
	        current_time = 0;
	        GameObject pl = GameObject.Find("Player");
            if(pl == null || username == null)
                return;

	        pl.GetComponentInChildren<Unit>().Nickname = username;
            pl.GetComponentInChildren<Unit>().IsGodMode = _isGodMode;
            standed = true;
	    }
	}

    public static SaveUsername GetInstance()
    {
        return _instance;
    }
}
