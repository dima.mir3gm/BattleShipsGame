﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GemsPool : MonoBehaviour
{
    public const string RED_GEM_KEY = "RED_GEM";
    public const string GRAY_GEM_KEY = "GREY_GEM";
    public const string ORANGE_GEM_KEY = "ORANGE_GEM";
    public const string BLUE_GEM_KEY = "BLUE_GEM";

    [SerializeField] private AbstractGem redGem;
    [SerializeField] private AbstractGem grayGem;
    [SerializeField] private AbstractGem orangeGem;
    [SerializeField] private AbstractGem blueGem;

    [SerializeField] private int count_red = 50;
    [SerializeField] private int count_grey = 50;
    [SerializeField] private int count_orange = 50;
    [SerializeField] private int count_blue = 20;

    private Dictionary<string, Dictionary<string, AbstractGem>> gems_list;
    private Dictionary<string, Dictionary<string, AbstractGem>> used_gems_list;
    private Dictionary<string, Dictionary<string, AbstractGem>> free_gems_list;


    public List<GameObject> Gems
    {
        get
        {
            List<GameObject> go = new List<GameObject>();
            foreach (var dc in gems_list.Values)
            {
                foreach (var gm in dc.Values)
                {
                    go.Add(gm.gameObject);
                }
            }
            return go;
        }
    } 

	void Awake () {
        gems_list = new Dictionary<string, Dictionary<string ,AbstractGem>>();
        used_gems_list = new Dictionary<string, Dictionary<string, AbstractGem>>();
        free_gems_list = new Dictionary<string, Dictionary<string, AbstractGem>>();

        Dictionary<string, Dictionary<string, AbstractGem>>[] tmp = {gems_list, used_gems_list, free_gems_list};
	    string[] keys = {RED_GEM_KEY, GRAY_GEM_KEY, ORANGE_GEM_KEY, BLUE_GEM_KEY};

	    foreach (var dict in tmp)
	    {
	        foreach (var key in keys)
	        {
	            dict.Add(key, new Dictionary<string, AbstractGem>());
	        }
	    }

	    int[] count_arr = {count_red, count_grey, count_orange, count_blue};
	    GameObject[] gems = {redGem.gameObject, grayGem.gameObject, orangeGem.gameObject, blueGem.gameObject};
	    int counter = 0;

	    foreach (var cnt in count_arr)
	    {
	        for (int i = 0; i < cnt; i++)
	        {
                GameObject gem = Instantiate(gems[counter]) as GameObject;
	            gem.transform.parent = transform;
                gem.transform.localPosition = Vector3.zero;
                gem.SetActive(false);
	            string id = gem.GetInstanceID().ToString();
	            gem.name = id;
                gems_list[keys[counter]].Add(id, gem.GetComponent<AbstractGem>());
	        }
	        counter++;
	    }

	}

    void Start()
    {
        PullMap();
    }

    public void Repull(GameObject obj)
    {
        obj.transform.position = GetComponentInParent<PositionManager>().GetPosition();
    }


    private void PullMap()
    {
        PositionManager manager = GetComponentInParent<PositionManager>();

        string[] keys = {RED_GEM_KEY, GRAY_GEM_KEY, ORANGE_GEM_KEY, BLUE_GEM_KEY};

        for (int i = 0; i < keys.Length; i++)
        {
            foreach (AbstractGem gem in gems_list[keys[i]].Values)
            {
                GameObject obj = gem.gameObject;
                obj.SetActive(true);
                used_gems_list[keys[i]].Add(obj.GetInstanceID().ToString(), gem);
                obj.transform.position = manager.GetPosition();
            }
        }

    }
	
}
