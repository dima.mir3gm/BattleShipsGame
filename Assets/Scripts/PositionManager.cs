﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PositionManager : MonoBehaviour
{

    [SerializeField] private Vector2 map_top_left;
    [SerializeField] private Vector2 map_bottom_right;
    [SerializeField] private GemsPool pool;

    private PositionGenerator generator;

	// Use this for initialization
	void Start () {
        PositionGenerator.initGenerator(pool.Gems, map_top_left, map_bottom_right);
	    generator = PositionGenerator.getGenerator();
	}

    public PositionGenerator GetGenerator()
    {
        return generator;
    }

    public Vector3 GetPosition()
    {
        return generator.Next();
    }

    public void AddObjects(List<GameObject> objects)
    {
        generator.AddObjects(objects);
    }

    public void AddObject(GameObject obj)
    {
        generator.AddObject(obj);
    }
}
