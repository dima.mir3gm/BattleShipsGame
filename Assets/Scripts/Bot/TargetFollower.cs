﻿using UnityEngine;
using System.Collections;

public class TargetFollower : MonoBehaviour, OnMoveParamsChanged, OnGemDestructed {
    public readonly Vector2 null_vector = new Vector2(-99999, -99999);
    private GameObject NULL_TARGET;

    [SerializeField] private GameObject target;
    [SerializeField] private float action_time = 0.2f;
    [SerializeField] private Vector2 useful_error = new Vector2(0.1f, 0.1f);
    [SerializeField] private Unit unit;
    [SerializeField] private GameObject blocked_target;
    [SerializeField] private float blocked_target_time = 1;
    private float blocked_tatrget_timer;

    private Vector2 temp_targ;
    private float velocity = 0;
    private float angulVelocity = 0;
    private float current_timer = 0;
    private TargetType current_target_type;

    // Use this for initialization
    void Start () {
        NULL_TARGET = GameObject.Find("NULL");
        target = NULL_TARGET;
        temp_targ = null_vector;
        unit.GemDestructedListener = this;
        velocity = unit.MovmentSpeed;
        angulVelocity = unit.TurnSpeed;
        current_target_type = TargetType.NULL;
    }
	
	// Update is called once per frame
	void Update ()
	{
	    velocity = unit.GetCurrentVelocity();
        angulVelocity = unit.TurnSpeed;

	    if (blocked_tatrget_timer < blocked_target_time)
	    {
	        blocked_tatrget_timer += Time.deltaTime;
	    }
	    else
	    {
	        blocked_target = NULL_TARGET;
	    }

        CheckTargetDie();
        MovmentControl();
    }

    private void MovmentControl()
    {
        if (current_target_type == TargetType.Enemy)
        {
            unit.Faster();
        }
        else
        {
            if(unit.GetCurrentSpeedCoef() > 1)
                unit.Slowly();
        }
    }

    private void CheckTargetDie()
    {
        if (current_target_type == TargetType.Enemy)
        {
            if (target.GetComponent<Unit>().IsDie)
            {
                target = NULL_TARGET;
                current_target_type = TargetType.NULL;
            }
            if (Vector3.Distance(target.gameObject.transform.position, transform.position) > 1200)
            {
                target = NULL_TARGET;
                current_target_type = TargetType.NULL;
            }
        }
    }

    public GameObject Target
    {
        set
        {
            if (target == NULL_TARGET && value != blocked_target)
                target = value;
        }

        get { return target; }
    }

    public void SetTarget(GameObject target, TargetType type)
    {
        if(current_target_type == TargetType.Enemy)
            return;
        else if(current_target_type == TargetType.Gem && type == TargetType.Enemy)
        {
            this.target = target;
            current_target_type = type;
        }
        else if(current_target_type == TargetType.NULL)
        {
            this.target = target;
            current_target_type = type;
        }
    }

    public RotateTypes1 MoveToTarget()
    {
        if(target == NULL_TARGET)
            return RotateTypes1.NoAction;

        if (temp_targ == null_vector)
        {
            return CheckPathToTarget();
        }
        else if (current_timer >= action_time)
        {
            current_timer = 0;
            temp_targ = null_vector;
            return CheckPathToTarget();
        }
        else
        {
            current_timer += Time.deltaTime;
            if (transform.position.x + useful_error.x > temp_targ.x &&
                transform.position.x - useful_error.x < temp_targ.x &&
                transform.position.y + useful_error.y > temp_targ.y &&
                transform.position.y - useful_error.y < temp_targ.y)
            {
                temp_targ = null_vector;
                current_timer = 0;
                return RotateTypes1.Cancel;
            }
            return RotateTypes1.NoAction;
        }
    }

    private RotateTypes1 CheckPathToTarget()
    {
        if (CheckLineMove())
        {
            return RotateTypes1.Cancel;
        }
        else
        {
            return CheckPosition();
        }
    }

    private bool CheckLineMove()
    {
        Vector2 dot_me = new Vector2(transform.position.x, transform.position.y);
        Vector2 dot_targ = new Vector2(target.transform.position.x, target.transform.position.y);

        float dist = Vector2.Distance(dot_me, dot_targ);

        Vector2 dot_new = new Vector2(dist * Mathf.Cos((transform.eulerAngles.z + 90 + 90) * Mathf.Deg2Rad), dist * Mathf.Sin((transform.eulerAngles.z + 90 + 90) * Mathf.Deg2Rad));

        if (dot_new.x + useful_error.x > dot_targ.x && dot_new.x - useful_error.x < dot_targ.x &&
            dot_new.y + useful_error.y > dot_targ.y && dot_new.y - useful_error.y < dot_targ.y)
        {
            Debug.Log("Target in line!");
            return true;
        }
        return false;
    }

    private RotateTypes1 CheckPosition()
    {
        float r = velocity / (angulVelocity * Mathf.PI / 180);
        //Debug.Log(r);
        Vector2 circle_center1 = new Vector2(transform.position.x - r * Mathf.Cos((transform.eulerAngles.z + 90 - 90) * Mathf.Deg2Rad),
            transform.position.y - r * Mathf.Sin((transform.eulerAngles.z + 90 - 90) * Mathf.PI / 180));


        Vector2 circle_center2 = new Vector2(transform.position.x - r * Mathf.Cos((transform.eulerAngles.z - 90 - 90) * Mathf.Deg2Rad),
            transform.position.y - r * Mathf.Sin((transform.eulerAngles.z - 90 - 90) * Mathf.PI / 180));


        //Debug.Log("Circle center 1 = " + circle_center1 + " Circle centr2 = " + circle_center2);

        Vector2 temp_targ1 = Vector2.MoveTowards(circle_center1, target.transform.position, r);
        Vector2 temp_targ2 = Vector2.MoveTowards(circle_center2, target.transform.position, r);
        //Instantiate(point, new Vector3(temp_targ1.x, temp_targ1.y), new Quaternion());
        //Instantiate(point, new Vector3(temp_targ2.x, temp_targ2.y), new Quaternion());
        float dist1 = Vector2.Distance(temp_targ1, target.transform.position);
        float dist2 = Vector2.Distance(temp_targ2, target.transform.position);

        //Debug.Log("Dist1 = " + dist1 + "    Dist 2 =" + dist2);

        
        if (dist1 == 0 || dist2 == 0)
        {
            return RotateTypes1.Cancel;
        }
        if (dist1 > dist2)
        {
            //Debug.Log("Right");
            temp_targ = temp_targ1;

            return RotateTypes1.Right;
        }
        else
        {
            //Debug.Log("Left");
            temp_targ = temp_targ2;
            //Instantiate(point, new Vector3(temp_targ.x, temp_targ.y), new Quaternion());
            return RotateTypes1.Left;

        }
    }

    public enum RotateTypes1
    {
        Left,
        Right,
        Cancel,
        NoAction
    }

    public enum TargetType 
    {
        Enemy,
        Gem,
        NULL
    }

    public void OnMoveParamsChanged(float velocity, float angularVelocity)
    {
        this.velocity = velocity;
        angulVelocity = angularVelocity;
    }

    public void OnGemDestructed(GameObject gem)
    {
        //Debug.Log("DESTROYED GEM ID " + gem.name);
        if (target == gem)
        {
            blocked_target = gem;
            blocked_tatrget_timer = 0;
            target = NULL_TARGET;
            current_target_type = TargetType.NULL;
        }
    }
}

public interface OnMoveParamsChanged
{
    void OnMoveParamsChanged(float velocity, float angularVelocity);
}
