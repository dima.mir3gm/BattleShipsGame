﻿using UnityEngine;
using System.Collections;
using System;

public class TwinCannos : Weapon
{

    public override void Attack()
    {
        if (BeforeAttack())
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).GetComponent<Weapon>().Attack();
            }
            AfterAttack();
        }
        
    }

    public override void SetPerfomance(float damage, float reload_speed, float range)
    {
        Damage = damage;
        ReloadSpeed = reload_speed;
        Range = range;
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).GetComponent<Weapon>().SetPerfomance(damage, reload_speed, range);
        }
    }
}