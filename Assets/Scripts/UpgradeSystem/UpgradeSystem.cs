﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpgradeSystem : MonoBehaviour
{

    public const int MAX_LVL = 2;

    [SerializeField] private PanelElementController side_panel;
    private bool sp_state;
    [SerializeField] private PanelElementController deck_panel;
    private bool dp_state;
    [SerializeField] private PanelElementController rear_panel;
    private bool rp_state;

    [SerializeField] private List<RectTransform> rear_panels;
    [SerializeField] private List<RectTransform> deck_panels;
    [SerializeField] private List<RectTransform> side_panels;

    [SerializeField] private Unit unit;
    [SerializeField] private CoinsScoreController controller;
    


    private string current_side_branch;
    private string current_deck_branch;
    private string current_rear_branch;

    private int side_lvl = 0;
    private int deck_lvl = 0;
    private int rear_lvl = 0;


    public void SidePanelClick()
    {
        bool tmp = rp_state;
        CloseAllPanels();
        sp_state = !tmp;
        if(sp_state)
        {

            if (current_side_branch == null)
            {
                side_panel.AddElement(side_panels[0]);
                side_panel.AddElement(side_panels[1]);
            }
            else
                SetPanel(current_side_branch, side_panel, side_panels);

        }
    }

    public void DeckPanelClick()
    {
        bool tmp = rp_state;
        CloseAllPanels();
        dp_state = !tmp;
        if (dp_state)
        {

            if (current_deck_branch == null)
            {
                deck_panel.AddElement(deck_panels[0]);
                deck_panel.AddElement(deck_panels[1]);
                deck_panel.AddElement(deck_panels[2]);
                deck_panel.AddElement(deck_panels[3]);
            }
            else
                SetPanel(current_deck_branch, deck_panel, deck_panels);

        }
    }

    public void RearPanelClick()
    {
        bool tmp = rp_state;
        CloseAllPanels();
        rp_state = !tmp;
        if (rp_state)
        {

            if (current_rear_branch == null)
            {
                rear_panel.AddElement(rear_panels[0]);
                rear_panel.AddElement(rear_panels[1]);
            }
            else
                SetPanel(current_rear_branch, rear_panel, rear_panels);

        }
    }

    private void CloseAllPanels()
    {
        rear_panel.Clear();
        rp_state = false;
        deck_panel.Clear();
        dp_state = false;
        side_panel.Clear();
        sp_state = false;
    }


    private void SetPanel(string current_branch , PanelElementController panel, List<RectTransform> panels )
    {
        Debug.Log(current_branch);
        foreach (var p in panels)
        {
            if (p.name.Equals(current_branch))
                panel.AddElement(p);
        }
    }

    private void CheckOnMaxLvl(int cur_lvl, PanelElementController cur_panel)
    {
        if (cur_lvl >= MAX_LVL)
        {
            cur_panel.transform.parent.gameObject.SetActive(false);
        }
    }




    public void OnRearCannonClick()
    {
        current_rear_branch = UpdateStructure.REAR_UPDATE[0];
        rear_lvl++;
        Upgrade(current_rear_branch, rear_lvl, UpgradeType.REAR, rear_panel);
        /*
        CloseAllPanels();
        CheckOnMaxLvl(rear_lvl, rear_panel);
        controller.Evolve();
        */
    }

    public void OnMineCannonClick()
    {
        current_rear_branch = UpdateStructure.REAR_UPDATE[1];
        rear_lvl++;
        Upgrade(current_rear_branch, rear_lvl, UpgradeType.REAR, rear_panel);
        /*
        current_rear_branch = UpdateStructure.REAR_UPDATE[1];
        rear_lvl++;
        CloseAllPanels();
        CheckOnMaxLvl(rear_lvl, rear_panel);
        controller.Evolve();
        */
    }

    public void OnBigCannonClick()
    {
        current_deck_branch = UpdateStructure.DECK_UPDATE[0];
        deck_lvl++;
        Upgrade(current_deck_branch, deck_lvl, UpgradeType.DECK, deck_panel);
        /*
        CloseAllPanels();
        CheckOnMaxLvl(deck_lvl, deck_panel);
        controller.Evolve();
        */
    }

    public void OnTwinCannonClick()
    {
        current_deck_branch = UpdateStructure.DECK_UPDATE[1];
        deck_lvl++;
        Upgrade(current_deck_branch, deck_lvl, UpgradeType.DECK, deck_panel);
        /*
        CloseAllPanels();
        CheckOnMaxLvl(deck_lvl, deck_panel);
        controller.Evolve();
        */
    }

    public void OnAutoCannonClick()
    {
        current_deck_branch = UpdateStructure.DECK_UPDATE[2];
        deck_lvl++;
        Upgrade(current_deck_branch, deck_lvl, UpgradeType.DECK, deck_panel);
        /*
        current_deck_branch = UpdateStructure.DECK_UPDATE[2];
        deck_lvl++;
        CloseAllPanels();
        CheckOnMaxLvl(deck_lvl, deck_panel);
        controller.Evolve();
        */
    }

    public void OnSwivelCannonClick()
    {
        /*
        current_deck_branch = UpdateStructure.DECK_UPDATE[3];
        deck_lvl++;

        CloseAllPanels();
        CheckOnMaxLvl(deck_lvl, deck_panel);
        controller.Evolve();
        */
    }

    public void OnScatterCannonClick()
    {
        current_side_branch = UpdateStructure.SIDE_UPDATE[0];
        side_lvl++;
        Upgrade(current_side_branch, side_lvl, UpgradeType.SIDE, side_panel);
        /*
        CloseAllPanels();
        CheckOnMaxLvl(side_lvl, side_panel);
        controller.Evolve();
        */
    }

    public void OnSideCannonClick()
    {
        current_side_branch = UpdateStructure.SIDE_UPDATE[1];
        side_lvl++;
        Upgrade(current_side_branch, side_lvl, UpgradeType.SIDE, side_panel);
        /*
        string name = UpdateStructure.GetBrunch(null, current_side_branch)[side_lvl - 1];
        Weapon weap =  (Instantiate(Resources.Load(name)) as GameObject).GetComponent<Weapon>();
        weap.gameObject.name = name;
        CloseAllPanels();
        CheckOnMaxLvl(side_lvl, side_panel);
        unit.Upgrade(UpgradeType.SIDE, weap);
        controller.Evolve();
        */
    }


    private void Upgrade(string cb, int lvl, UpgradeType type, PanelElementController panel)
    {
        string name = UpdateStructure.GetBrunch(null, cb)[lvl - 1];
        Weapon weap = (Instantiate(Resources.Load(name)) as GameObject).GetComponent<Weapon>();
        weap.gameObject.name = name;
        CloseAllPanels();
        CheckOnMaxLvl(lvl, panel);
        unit.Upgrade(type, weap);
        controller.Evolve();
    }

}


public enum UpgradeType
{
    SIDE,
    DECK,
    REAR
}
