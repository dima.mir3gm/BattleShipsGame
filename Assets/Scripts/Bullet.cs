﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float destruction_delay = 0.1f;
    private float bullet_life;
    private BulletPool pool;
    private float damage;
    private Unit parent;

    private float currentTime;

    public float BulletLife
    {
        get { return bullet_life; }
        set { bullet_life = value; }
    }

    public BulletPool Pool
    {
        get { return pool; }
        set { pool = value; }
    }

    public float Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    public Unit Parent
    {
        set { parent = value; }
        get { return parent; }
    }

    public void Destroy()
    {
        currentTime = bullet_life - destruction_delay;
    }

    void Update()
    {
        if(pool == null)
            return;
        if (currentTime < bullet_life)
        {
            currentTime += Time.deltaTime;
        }
        else
        {
            currentTime = 0;
            pool.DestroyBullet(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        string tag = col.tag;
        if (col.gameObject == parent.gameObject)
        {
        }
        else if(col.GetComponent<Unit>() != null)
        {
            col.GetComponent<Unit>().Hit(parent , damage);
            Destroy();
        }
        else if(tag.Equals("Bullet") && parent != col.GetComponent<Bullet>().parent)
        {
            col.GetComponent<Bullet>().Destroy();
            Destroy();            
        }
    }
}
