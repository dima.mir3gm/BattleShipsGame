﻿using UnityEngine;
using System.Collections;

public class ErrorControlPanels : MonoBehaviour {

    [SerializeField]
    private GameObject serverBusyPanel;

    [SerializeField]
    private GameObject errorPanel;

    private PanelsController pc;

	// Use this for initialization
	void Start ()
	{
	    pc= GetComponentInParent<PanelsController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void showErrorPanel()
    {
        hideAllPanels();
        errorPanel.SetActive(true);
    }

    public void showServerBusyPanel()
    {
        hideAllPanels();
        serverBusyPanel.SetActive(true);
    }


    public void hideAllPanels()
    {
        serverBusyPanel.SetActive(false);
        errorPanel.SetActive(false);
    }

    public void onCancelClick()
    {
        hideAllPanels();
        GetComponentInParent<PanelsController>().OffPanel(2);
    }

    public void onAgain()
    {
        pc.OffPanel(1);
        pc.OnPanel(0);
    }

}
