﻿using UnityEngine;
using System.Collections;

public class UpgradeStatusLoadController : MonoBehaviour
{

    [SerializeField] private RectTransform load;
    [SerializeField] private float x_indent = -20;
    [SerializeField] private float y_indent = -20;
    private float size_x;
    private float size_y;

    void Start()
    {
        size_x = load.rect.size.x;
        size_y = load.rect.size.y;
    }

    public void SetPercentValue(float val)
    {
        load.sizeDelta = new Vector2(x_indent - size_x * (1 - val/100), y_indent);
    }


}
