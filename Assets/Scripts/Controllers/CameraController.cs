﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

    [SerializeField] private Unit unit;
	
	// Update is called once per frame
	void Update () {
        if(unit != null)
	        transform.position = new Vector3(unit.transform.position.x, unit.transform.position.y, transform.position.z);
	}
}
