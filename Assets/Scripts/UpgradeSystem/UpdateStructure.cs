﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpdateStructure {

    public static readonly string[] UPDATE_TYPES =
    {
        "Rear",
        "Deck",
        "Side"
    };

    public static readonly string[] REAR_UPDATE =
    {
        "RearCannon",
        "MineCannon"
    };

    public static readonly string[] DECK_UPDATE =
    {
        "BigCannon",
        "TwinCannon",
        "AutoCannon",
        "SwivelCannon"
    };

    public static readonly string[] SIDE_UPDATE =
    {
        "ScatterCannon",
        "SideCannon"
    };

    public static readonly Dictionary<string, string[]> UPDATE_TREE = new Dictionary<string, string[]>();

    static UpdateStructure()
    {
        UPDATE_TREE.Add(SIDE_UPDATE[0], new string[]
        {
            "ScatterCannon1",
            "ScatterCannon2"
        });

        UPDATE_TREE.Add(SIDE_UPDATE[1], new string[]
        {
            "SideCannon1",
            "SideCannon2"
        });

        UPDATE_TREE.Add(DECK_UPDATE[0], new string[]
        {
            "BigCannon1",
            "BigCannon2"
        });

        UPDATE_TREE.Add(DECK_UPDATE[1], new string[]
        {
            "TwinCannon1",
            "TwinCannon2"
        });

        UPDATE_TREE.Add(DECK_UPDATE[2], new string[]
        {
            "AutoCannon1",
            "AutoCannon2"
        });

        UPDATE_TREE.Add(DECK_UPDATE[3], new string[]
        {
            "SwivelCannon1",
            "SwivelCannon2"
        });

        UPDATE_TREE.Add(REAR_UPDATE[0], new string[]
        {
            "RearCannon1",
            "RearCannon2"
        });

        UPDATE_TREE.Add(REAR_UPDATE[1], new string[]
        {
            "MineCannon1",
            "MineCannon2"
        });
    }

    public static string[] GetBrunch(string type_key, string branch_key)
    {
        if (type_key == null && branch_key == null)
            throw new System.Exception();

        if(branch_key == null)
        {
            if(type_key == UPDATE_TYPES[0])
            {
                return CopyArray(REAR_UPDATE);
            }
            else if(type_key == UPDATE_TYPES[1])
            {
                return CopyArray(DECK_UPDATE);
            }
            else if(type_key == UPDATE_TYPES[2])
            {
                return CopyArray(SIDE_UPDATE);
            }
            throw new System.Exception();
        }

        return UPDATE_TREE[branch_key];
    }


    private static string[] CopyArray(string[] arr)
    {
        string[] tmp = new string[arr.Length];
        tmp = (string[])arr.Clone();
        return tmp;
    }
}
