﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D), typeof(Rigidbody2D))]
public abstract class AbstractGem : MonoBehaviour
{

    [SerializeField] protected float expiriance;
    [SerializeField] protected GemsPool pool;
    protected float coef = 1f;

    void Start()
    {
        coef = SaveUsername.GetInstance().ExpCoef;
    }

    public float Expiriance
    {
        get { return expiriance; }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        Unit unit = col.GetComponent<Unit>();
        if (unit != null)
        {
            float exp = expiriance;
            if (unit.transform.parent.name.Equals("Player"))
                exp = expiriance*coef;
            unit.AddExpiriance(exp);
            unit.GemDestructed(gameObject);
            pool.Repull(gameObject);
        }
    }
}
