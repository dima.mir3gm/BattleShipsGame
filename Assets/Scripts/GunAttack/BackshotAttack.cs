﻿using UnityEngine;
using System.Collections;

public class BackshotAttack : GunAttack
{

    [SerializeField] private Vector3 TOP_START_VECTOR = new Vector3(7, 7, 0);
    [SerializeField] private Vector3 LOW_START_VECTOR = new Vector3(7, -7, 0);
    [SerializeField] private Vector3 BULLET_SIZE = new Vector3(0.4f, 0.4f, 0);

    private Unit unit;

    protected override void Start()
    {
        base.Start();
        Transform t = transform;
        while (unit == null)
        {
            unit = t.GetComponent<Unit>();
            t = t.parent;
        }
    }

    public override void Fire(float damage, float range)
    {
        base.Fire(damage,range);

        //top gun
        if(pool == null)
            return;;
            
        GameObject bullet1 = pool.InstantiateBullet();
        bullet1.transform.parent = transform;
        bullet1.transform.localPosition = TOP_START_VECTOR;
        float angle1 = ((-transform.parent.parent.eulerAngles.z + transform.parent.eulerAngles.z + transform.eulerAngles.z +45) * Mathf.PI) / 180;
        bullet1.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity_powering * Mathf.Cos(angle1), velocity_powering * Mathf.Sin(angle1));
        bullet1.transform.parent = null;
        bullet1.transform.localScale = BULLET_SIZE;
        Bullet bul1 = bullet1.GetComponent<Bullet>();
        bul1.BulletLife = range;
        bul1.Parent = unit;
        bul1.Damage = damage;


        //low gun
        GameObject bullet2 = pool.InstantiateBullet();
        bullet2.transform.parent = transform;
        bullet2.transform.localPosition = TOP_START_VECTOR;
        float angle2 = ((-transform.parent.parent.eulerAngles.z + transform.parent.eulerAngles.z + transform.eulerAngles.z - 45) * Mathf.PI) / 180;
        bullet2.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity_powering * Mathf.Cos(angle2), velocity_powering * Mathf.Sin(angle2));
        bullet2.transform.parent = null;
        bullet2.transform.localScale = BULLET_SIZE;
        Bullet bul2 = bullet2.GetComponent<Bullet>();
        bul2.BulletLife = range;
        bul2.Parent = unit;
        bul2.Damage = damage;
    }
}
