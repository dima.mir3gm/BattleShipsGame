﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MinePool : MonoBehaviour {

    [SerializeField] private GameObject mines;
    [SerializeField] private int pool_Size = 1000;

    private int mines_counter;
    private int used_mines;

    private List<GameObject> mines_pool;
    private List<GameObject> free_mines;
    private List<GameObject> using_mines;

    void Awake()
    {
        mines_pool = new List<GameObject>(pool_Size);
        free_mines = new List<GameObject>(pool_Size);
        using_mines = new List<GameObject>(pool_Size);
        CreateMines(pool_Size);
    }

    public GameObject InstantiateMine()
    {
        if (free_mines.Count == 0)
        {
            CreateMines(pool_Size / 10);
        }
        GameObject bullet1 = free_mines[0];
        bullet1.transform.position = Vector3.zero;
        bullet1.SetActive(true);
        free_mines.Remove(bullet1);
        using_mines.Add(bullet1);

        used_mines = using_mines.Count;
        return bullet1;
    }

    private void CreateMines(int count)
    {
        mines_counter += count;
        for (int i = 0; i < count; i++)
        {
            GameObject bullet1 = Instantiate(mines) as GameObject;
            bullet1.SetActive(false);
            bullet1.transform.position = transform.position;
            bullet1.transform.parent = transform;
            bullet1.name = bullet1.GetInstanceID().ToString();
            bullet1.GetComponent<Mine>().Pool = this;

            mines_pool.Add(bullet1);
            free_mines.Add(bullet1);
        }
    }

    public void DestroyMine(GameObject obj)
    {
        using_mines.Remove(obj);
        free_mines.Add(obj);
        obj.SetActive(false);
        obj.transform.position = transform.position;
        obj.transform.parent = transform;
        obj.transform.localScale = Vector3.one;
    }
}
