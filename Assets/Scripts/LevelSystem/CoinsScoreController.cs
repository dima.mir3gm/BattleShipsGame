﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoinsScoreController : MonoBehaviour, OnExpStateChanged, OnEvolutionLevelChanged, OnEvolutionProcess
{
    [SerializeField] private LevelSystem levelSystem;
    [SerializeField] private Text score;
    [SerializeField] private Text coins;

    [SerializeField] private GameObject upgradePanel;
    [SerializeField] private UpgradeStatusLoadController load_controller;

    private int curent_evol_lvl;
    private int evol_coin;

    void Start()
    {
        levelSystem.AddExpEventHandler(this);
        levelSystem.AddEvolUpdatedHandler(this);
        levelSystem.AddEvolProcessHandler(this);
        score.text = levelSystem.CurrentRaitingPoints.ToString();
        coins.text = levelSystem.CurrentCoins.ToString();
    }

    public void Evolve()
    {
        evol_coin--;
        if (evol_coin == 0)
        {
            upgradePanel.SetActive(false);
            load_controller.gameObject.SetActive(true);
        }
    }

    public void ExpStateChanged(float exp, float coins, float raiting_points)
    {
        this.coins.text = coins.ToString();
        score.text = raiting_points.ToString();
    }

    public void OnEvolutionLevelChanged(int lvl)
    {
        evol_coin += lvl - curent_evol_lvl;
        curent_evol_lvl = lvl;
        if (evol_coin > 0)
        {
            upgradePanel.SetActive(true);
            load_controller.gameObject.SetActive(false);
        }
    }

    public void OnEvolutionProcess(float percent_value)
    {
        load_controller.SetPercentValue(percent_value);
    }
}
