﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendGodMode : MonoBehaviour {

    public bool IsGodMode
    {
        set { SaveUsername.GetInstance().IsGodMode = value; }
    }
}
