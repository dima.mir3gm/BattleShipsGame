﻿using UnityEngine;
using System.Collections;

public class TimingMove : MonoBehaviour
{
    public readonly Vector2 null_vector = new Vector2(-99999, -99999);

    [SerializeField] private GameObject target;
    [SerializeField] private GameObject point;

    [SerializeField] private float velocity = 1;
    [SerializeField] private float angulVelocity = 10;
    [SerializeField] private float action_time = 0.2f;
    private float current_vilocity;
    private float current_timer = 0;
    private bool isStart;

    [SerializeField] private Vector2 useful_error = new Vector2(0.1f, 0.1f);

    private Rigidbody2D rb;
    private float old_angle = - 9999;
    private RotateTypes current_action;
    private Vector2 temp_targ;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        temp_targ = null_vector;
        current_action = RotateTypes.NoAction;
        isStart = true;
    }

    void Update()
    {
        Move();
        MoveToTarget();
    }

    private void RecalcVelocity()
    {
        rb.velocity = new Vector2(velocity * Mathf.Cos(transform.eulerAngles.z * Mathf.PI / 180), velocity * Mathf.Sin(transform.eulerAngles.z * Mathf.PI / 180));
    }

    private void ActivateRoate(RotateTypes type)
    {
        current_action = type;
        switch (type)
        {
            case RotateTypes.Right:
                rb.angularVelocity = angulVelocity;
                current_vilocity = angulVelocity;
                break;

            case RotateTypes.Left:
                rb.angularVelocity = -angulVelocity;
                current_vilocity = -angulVelocity;
                break;

            case RotateTypes.Cancel:
                rb.angularVelocity = 0;
                current_action = RotateTypes.NoAction;
                break;

            case RotateTypes.NoAction:
                break;
        }
    }

    private void Move()
    {
        if (rb.angularVelocity != 0 || isStart)
        {
            RecalcVelocity();
        }
    }

    private void MoveToTarget()
    {
        if (temp_targ == null_vector)
        {
            ActivateRoate(CheckPathToTarget());
        }
        else if(current_timer >= action_time)
        {
            current_timer = 0;
            temp_targ = null_vector;
            ActivateRoate(CheckPathToTarget());
        }
        else
        {
            current_timer += Time.deltaTime;
            if (transform.position.x + useful_error.x > temp_targ.x &&
                transform.position.x - useful_error.x < temp_targ.x &&
                transform.position.y + useful_error.y > temp_targ.y &&
                transform.position.y - useful_error.y < temp_targ.y)
            {
                temp_targ = null_vector;
                current_timer = 0;
            }
        }
    }

    private RotateTypes CheckPathToTarget()
    {
        if (CheckLineMove())
        {
            return RotateTypes.Cancel;
        }
        else
        {
            return CheckPosition();
        }
    }

    private bool CheckLineMove()
    {
        Vector2 dot_me = new Vector2( transform.position.x, transform.position.y);
        Vector2 dot_targ = new Vector2(target.transform.position.x, target.transform.position.y);

        float dist = Vector2.Distance(dot_me, dot_targ);

        Vector2 dot_new = new Vector2(dist * Mathf.Cos((transform.eulerAngles.z + 90) * Mathf.Deg2Rad), dist * Mathf.Sin((transform.eulerAngles.z + 90) * Mathf.Deg2Rad));

        if (dot_new.x + useful_error.x > dot_targ.x && dot_new.x - useful_error.x < dot_targ.x &&
            dot_new.y + useful_error.y > dot_targ.y && dot_new.y - useful_error.y < dot_targ.y)
        {
            Debug.Log("Target in line!");
            return true;
        }
        return false;
    }

    private RotateTypes CheckPosition()
    {
        float r = velocity/(angulVelocity * Mathf.PI / 180);
        Vector2 circle_center1 = new Vector2(transform.position.x - r*Mathf.Cos((transform.eulerAngles.z + 90) * Mathf.Deg2Rad),
            transform.position.y - r*Mathf.Sin((transform.eulerAngles.z + 90 ) * Mathf.PI / 180));


        Vector2 circle_center2 = new Vector2(transform.position.x - r * Mathf.Cos((transform.eulerAngles.z - 90) * Mathf.Deg2Rad),
            transform.position.y - r * Mathf.Sin( (transform.eulerAngles.z - 90 )* Mathf.PI / 180));

        Vector2 temp_targ1 = Vector2.MoveTowards(circle_center1, target.transform.position, r);
        Vector2 temp_targ2 = Vector2.MoveTowards(circle_center2, target.transform.position, r);
        float dist1 = Vector2.Distance(temp_targ1, target.transform.position);
        float dist2 = Vector2.Distance(temp_targ2, target.transform.position);

        if (dist1 == 0 || dist2 == 0)
        {
            return RotateTypes.Cancel;
        }
        if (dist1 > dist2)
        {
            temp_targ = temp_targ1;
            return RotateTypes.Right;
            
        }
        else
        {
            temp_targ = temp_targ2;
            return RotateTypes.Left;
        }
    }

    public enum RotateTypes
    {
        Left,
        Right,
        Cancel,
        NoAction
    }

}
