﻿using System;
using UnityEngine;
using System.Collections;

public class TriggerTest : MonoBehaviour
{
    private const string FOG1 = "FOG_1";
    private const string FOG2 = "FOG_2";
    private const string FOG3 = "FOG_3";
    private const string FOG4 = "FOG_4";

    private float coef_x;
    private float coef_y;


    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnTriggerStay2D(Collider2D col)
    {
        //calcPhys(col);
    }

    void OnTriggerExit2D(Collider2D col)
    {
        //calcPhys(col);
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        calcPhys(col);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        
        Debug.Log("Collision");
        float vel_x = 0;
        float col_z = col.transform.eulerAngles.z;

        switch (name)
        {
            case FOG1:
            {
                if (col_z < 90 || col_z > 270)
                    vel_x = -1;
                else
                    vel_x = 1;

                coef_y = 1;
                coef_x = 0;
                break;
            }

            case FOG2:
                {
                    if (col_z < 90 || col_z > 270)
                        vel_x = 1;
                    else
                    {
                        vel_x = -1;
                    }
                    coef_y = 1;
                    coef_x = 0;
                    break;
                }

            case FOG3:
                {
                    if (col_z >= 0 && col_z < 180)
                        vel_x = 1;
                    else
                    {
                        vel_x = -1;
                    }
                    coef_y = 0;
                    coef_x = 1;
                    break;
                }

            case FOG4:
                {
                    if (col_z >= 0 && col_z < 180)
                        vel_x = -1;
                    else
                    {
                        vel_x = 1;
                    }
                    coef_y = 0;
                    coef_x = 1;
                    break;
                }
        }

        Rigidbody2D rb = col.gameObject.GetComponent<Rigidbody2D>();
        Unit unit = col.gameObject.GetComponent<Unit>();
        unit.BlockMove = true;
        rb.velocity = new Vector2(unit.MovmentSpeed * vel_x * coef_x, unit.MovmentSpeed * vel_x * coef_y);
        Debug.Log(vel_x);
        rb.angularVelocity = unit.TurnSpeed * vel_x;
        
    }

    void OnCollisionExit2D(Collision2D col)
    {
        
        Debug.Log("Uncollision");
        Unit unit = col.gameObject.GetComponent<Unit>();
        unit.BlockMove = false;
        

    }

    private void calcPhys(Collider2D col)
    {
        Debug.Log("Triggered");
        Vector3 pos = col.gameObject.transform.position;
        //Vector3 pos_1 = transform.position;

        col.gameObject.transform.position = col.GetComponent<Unit>().Old_pos;
        /*
        switch (name)
        {
            case FOG1:
                {
                    col.gameObject.transform.position = new Vector3(-4500, pos.y, pos.z);
                    break;
                }

            case FOG2:
                {
                    col.gameObject.transform.position = new Vector3(4500, pos.y, pos.z);
                    break;
                }

            case FOG3:
                {
                    col.gameObject.transform.position = new Vector3(pos.x, 4500, pos.z);
                    break;
                }

            case FOG4:
                {
                    col.gameObject.transform.position = new Vector3(pos.x, -4500, pos.z);
                    break;
                }
        }
        */
    }
}
