﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;

public class LeaderBoardController : MonoBehaviour, OnScoreChangedScoreboard
{
    public const string DEVIDE_SYMBOL = " : ";

    [SerializeField] private List<Text> leader_text_fields;
    private Dictionary<string, ScoreClass> units;

    void Start()
    {
        units = new Dictionary<string, ScoreClass>();
        Unit.SetScoreboardListener(this);
    }

    public void OnScoreChanged(float score, Unit unit)
    {
        string id = unit.gameObject.GetInstanceID().ToString();

        if (units.ContainsKey(id))
        {
            units[id].Score = score;
        }
        else
        {
            units.Add(id, new ScoreClass(score, unit));
        }
        RecalculateBoard();
    }

    public void RecalculateBoard()
    {
        var values = units.Values.ToList();

        for (int i = 0; i < values.Count - 1; i++)
        {
            for (int j = 0; j < values.Count - 1; j++)
            {
                if (values[j + 1].Score > values[j].Score)
                {
                    ScoreClass tmp = values[j];
                    values[j] = values[j + 1];
                    values[j + 1] = tmp;
                }
            }
        }
        showOnBoard(values);

    }

    private void showOnBoard(List<ScoreClass> players)
    {

        if (players.Count >= leader_text_fields.Count)
        {
            for (int i = 0; i < leader_text_fields.Count; i++)
            {
                leader_text_fields[i].gameObject.SetActive(true);
                leader_text_fields[i].text = new StringBuilder().Append(players[i].Unit.Nickname).Append(DEVIDE_SYMBOL).Append(players[i].Score).ToString();
            }
        }
        else
        {
            int i;
            for (i = 0; i < players.Count; i++)
            {
                leader_text_fields[i].gameObject.SetActive(true);
                leader_text_fields[i].text = new StringBuilder().Append(players[i].Unit.Nickname).Append(DEVIDE_SYMBOL).Append(players[i].Score).ToString();
            }
            for (; i < leader_text_fields.Count; i++)
            {
                leader_text_fields[i].gameObject.SetActive(false);
            }
        }
    }


    private class ScoreClass
    {

        private float score;
        private Unit unit;
        private int possition;

        public ScoreClass(float score, Unit unit)
        {
            this.score = score;
            this.unit = unit;
        }

        public float Score
        {
            set { score = value; }
            get { return score; }
        }

        public Unit Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        public int Possition
        {
            get { return possition; }
            set { possition = value; }
        }
    }
}
