﻿using UnityEngine;
using System.Collections;

public abstract class Weapon : MonoBehaviour
{

    [SerializeField] protected float damage_coef;
    [SerializeField] protected float reload_speed_coef;

    private float damage;
    private float reload_speed;
    private float range;

    private float timer;
    private bool canAttack = true;

    public float Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    public float ReloadSpeed
    {
        get { return reload_speed; }
        set { reload_speed = value; }
    }

    public float Range
    {
        get { return range; }
        set { range = value; }
    }

    public bool CanAttack
    {
        get { return canAttack; }
    }

    public bool BeforeAttack()
    {
        return canAttack;
    }

    public void AfterAttack()
    {
        canAttack = false;
    }

    // Update is called once per frame
    protected virtual void Update () {
	    if (!canAttack)
	    {
	        if (timer < 1/reload_speed)
	        {
	            timer += Time.deltaTime;
	        }

	        if (timer >= 1/reload_speed)
	        {
	            canAttack = true;
	            timer = 0;
	        }
	    }
	}


    public abstract void Attack();

    public abstract void SetPerfomance(float damage, float reload_speed, float range);
}
