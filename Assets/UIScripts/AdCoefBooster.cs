﻿using UnityEngine;
using System.Collections;
using AppodealAds.Unity.Common;
using UnityEngine.UI;

public class AdCoefBooster : MonoBehaviour, IRewardedVideoAdListener
{

    private AdController adController;
    private Button rewardAdButton;

    private void Start()
    {
        adController = FindObjectOfType<AdController>();
        if(adController) adController.AddListener(this);
        rewardAdButton = GetComponent<Button>();
    }

    private void Update()
    {
        if (adController && rewardAdButton) rewardAdButton.interactable = adController.isRewardVideo();
    }

    public void WatchAdAndBoostCoef()
    {
        if (adController)
        {
            adController.ShowRewardVideo();
        }
    }

    public void onRewardedVideoLoaded()
    {
    }

    public void onRewardedVideoFailedToLoad()
    {
    }

    public void onRewardedVideoShown()
    {
    }

    public void onRewardedVideoFinished(int amount, string name)
    {
        SaveUsername.GetInstance().UpCoeficient();
    }

    public void onRewardedVideoClosed()
    {
    }
}
