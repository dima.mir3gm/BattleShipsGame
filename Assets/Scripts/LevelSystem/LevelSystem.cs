﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

public class LevelSystem : MonoBehaviour
{
    [SerializeField] private float start_expiriance;
    [SerializeField] private int start_summary_lvl = 0;
    [SerializeField] private bool isBot;
    [SerializeField] private float bot_autoupgrade_timer = 15;
    private float timer;

    public const string AMMO_HP = "1";
    public const string AMMO_HP_REGEN = "2";
    public const string AMMO_CANON_RANGE = "3";
    public const string AMMO_CANON_DAMAGE = "4";
    public const string AMMO_RELOAD_SPEED = "5";
    public const string AMMO_MOVMENT_SPEED = "6";
    public const string AMMO_TURN_SPEED = "7";
    public const string AMMO_RAM_DAMAGE = "8";

    public const int START_AMMO_LVL = 0;

    private List<OnLevelChanged> handlers;
    private List<OnExpStateChanged> board_handlers;
    private List<OnSummaryLevelUpped> level_board_handlers;
    private List<OnStateCanBeUpdated> state_updated_handler;
    private List<OnEvolutionLevelChanged> evol_updatel_handler;
    private List<OnEvolutionProcess> evol_processed_handler;
    private List<OnScoreChanged> score_handler; 

    public readonly string[] ammo_keys =
    {
        AMMO_HP, AMMO_HP_REGEN, AMMO_CANON_RANGE, AMMO_CANON_DAMAGE, AMMO_RELOAD_SPEED,
        AMMO_MOVMENT_SPEED, AMMO_TURN_SPEED, AMMO_RAM_DAMAGE
    };

    private float current_exp;
    private float current_coins;
    private float current_raiting_points;

    private int summary_level = 0;

    private int evolution_lvl = 0;

    public float CurrentExp
    {
        get { return current_exp; }
    }

    public float CurrentCoins
    {
        get { return current_coins; }
    }

    public float CurrentRaitingPoints
    {
        get { return current_raiting_points; }
    }

    private Dictionary<string, int> ammo_level_tree;

    private int current_upgrade_lvl;

    public LevelSystem()
    {
        ammo_level_tree = new Dictionary<string, int>();
        handlers = new List<OnLevelChanged>();
        board_handlers = new List<OnExpStateChanged>();
        level_board_handlers = new List<OnSummaryLevelUpped>();
        state_updated_handler = new List<OnStateCanBeUpdated>();
        evol_updatel_handler = new List<OnEvolutionLevelChanged>();
        evol_processed_handler = new List<OnEvolutionProcess>();
        score_handler = new List<OnScoreChanged>();
        foreach (var k in ammo_keys)
        {
            ammo_level_tree.Add(k, 0);
        }
    }

    void Start()
    {
        foreach (var k in ammo_keys)
        {
            LevelChangedAction(k, 0);
        }
        summary_level = start_summary_lvl;
        Expiriance = start_expiriance;
        UpLevel();
        timer = bot_autoupgrade_timer - 1;
    }

    void Update()
    {
        if (isBot && summary_level < 80)
        {
            if (timer >= bot_autoupgrade_timer)
            {
                timer = 0;
                UpLevel();
            }
            else
            {
                timer += Time.deltaTime;
            }
        }
    }

    private void BotUpdate()
    {

        UpLevel();
    }

    public float Expiriance
    {
        set
        {
            current_exp += value;
            current_coins += value;
            current_raiting_points += value;
            if (!isBot)
            {
                CheckUpgradeProcess();
                ScoreChangedNotify();
                CheckEvolution();
                ExpStateChanged(current_exp, current_coins, current_raiting_points);
                StateUpdate();
            }
            else
            {
                CheckUpgradeProcess();
                ScoreChangedNotify();
                ExpStateChanged(current_exp, current_coins, current_raiting_points);
                StateUpdate();
            }
        }
    }


    public void CheckUpgradeProcess()
    {
        if(evolution_lvl >= LevelStructure.GetUpgradeStructure().Length)
            return;
        foreach (var listener in evol_processed_handler)
        {
            listener.OnEvolutionProcess((current_exp/ LevelStructure.GetUpgradeStructure()[evolution_lvl + 1]) * 100);
        }

    }

    public bool[] CheckAmmosUpgrade()
    {
        bool[] mask = new bool[ammo_level_tree.Count];

        int i = 0;
        foreach (var key in ammo_level_tree.Keys)
        {
            if (CheckAmmoUpgrade(key))
                mask[i] = true;
            else
                mask[i] = false;

            i++;
        }

        return mask;

    }

    public bool CheckAmmoUpgrade(string key)
    {
        if (ammo_level_tree.ContainsKey(key))
        {
            int max_lvl = LevelStructure.GetLevelStructure().Length;
            if (max_lvl > ammo_level_tree[key] + 1 && current_coins >= LevelStructure.GetLevelStructure()[ammo_level_tree[key] + 1])
            {
                return true;
            }

            return false;
        }

        throw new Exception();
    }

    public bool UpgradeAmmoLvl(string key)
    {
        if (CheckAmmoUpgrade(key))
        {
            int c_l = ammo_level_tree[key] + 1;
            current_coins -= LevelStructure.GetLevelStructure()[c_l];
            ammo_level_tree[key] = c_l;
            LevelChangedAction(key, c_l);
            ExpStateChanged();
            UpLevel();
            StateUpdate();
            return true;
        }
        return false;
    }

    public void CheckEvolution()
    {
        float need_exp = LevelStructure.GetUpgradeStructure()[evolution_lvl + 1];
        if (current_exp >= need_exp)
        {
            current_exp -= need_exp;
            evolution_lvl++;
            foreach (var listener in evol_updatel_handler)
            {
                listener.OnEvolutionLevelChanged(evolution_lvl);   
            }

        }
    }

    private void ScoreChangedNotify()
    {
        foreach (var listener in score_handler)
        {
            listener.OnScoreChanged(current_raiting_points);
        }
    }

    public void AddLevelEventHandler(OnLevelChanged handl)
    {
        handlers.Add(handl);
    }

    public void RemoveLevelEventHandler(OnLevelChanged handl)
    {
        handlers.Remove(handl);
    }

    public void AddScoreHandler(OnScoreChanged listener)
    {
        score_handler.Add(listener);
    }

    public void RemoveScoreHandler(OnScoreChanged listener)
    {
        score_handler.Remove(listener);
    }

    public void AddExpEventHandler(OnExpStateChanged listener)
    {
        board_handlers.Add(listener);
    }

    public void RemoveExpEventHandler(OnExpStateChanged listener)
    {
        board_handlers.Remove(listener);
    }

    public void AddSummaryLevelEventHandler(OnSummaryLevelUpped listener)
    {
        level_board_handlers.Add(listener);
    }

    public void RemoveSummaryLevelEventHandler(OnSummaryLevelUpped listener)
    {
        level_board_handlers.Remove(listener);
    }

    public void AddStateHandler(OnStateCanBeUpdated listener)
    {
        state_updated_handler.Add(listener);
    }

    public void RemoveStateHandler(OnStateCanBeUpdated listener)
    {
        state_updated_handler.Remove(listener);
    }

    public void AddEvolUpdatedHandler(OnEvolutionLevelChanged listener)
    {
        evol_updatel_handler.Add(listener);
    }

    public void RemoveEvolUpdatedHandler(OnEvolutionLevelChanged listener)
    {
        evol_updatel_handler.Remove(listener);
    }

    public void AddEvolProcessHandler(OnEvolutionProcess listener)
    {
        evol_processed_handler.Add(listener);
    }

    public void RemoveEvolProcessHandler(OnEvolutionProcess listener)
    {
        evol_processed_handler.Remove(listener);
    }

    private void LevelChangedAction(string key, int lvl)
    {
        foreach (var listener in handlers)
        {
            listener.LevelChanged(key, lvl);   
        }
    }

    private void ExpStateChanged(float exp, float coins, float raiting_points)
    {
        foreach (var listener in board_handlers)
        {
            listener.ExpStateChanged(exp, coins, raiting_points);
        }
    }

    private void ExpStateChanged()
    {
        foreach (var listener in board_handlers)
        {
            listener.ExpStateChanged(current_exp, current_coins, current_raiting_points);
        }
    }

    private void UpLevel()
    {
        foreach (var listener in level_board_handlers)
        {
            listener.SummaryLevelUpped(++summary_level);
        }
    }

    private void StateUpdate()
    {
        bool[] states = new bool[ammo_keys.Length];
        int i = 0;
        foreach (var key in ammo_keys)
        {
            states[i++] = CheckAmmoUpgrade(key);
        }

        foreach (var listener in state_updated_handler)
        {
            listener.OnStateCanBeUpdated(states);
        }
    }

}

public interface OnLevelChanged
{
    void LevelChanged(string key, int lvl);
}

public interface OnExpStateChanged
{
    void ExpStateChanged(float exp, float coins, float raiting_points);
}

public interface OnSummaryLevelUpped
{
    void SummaryLevelUpped(int current_level);
}

public interface OnStateCanBeUpdated
{
    void OnStateCanBeUpdated(bool[] states);
}

public interface OnEvolutionLevelChanged
{
    void OnEvolutionLevelChanged(int lvl);
}

public interface OnEvolutionProcess
{
    void OnEvolutionProcess(float percent_value);
}

public interface OnScoreChanged
{
    void OnScoreChanged(float score);
}
