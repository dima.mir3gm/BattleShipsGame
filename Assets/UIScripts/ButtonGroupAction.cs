﻿using UnityEngine;
using System.Collections;

public class ButtonGroupAction : MonoBehaviour {

    public void Rate()
    {
        Application.OpenURL("market://details?id=" + Application.bundleIdentifier);
        FindObjectOfType<AnalyticsController>().LogEvent("Rate", "Rate");
    }

    public void Share()
    {
        AndroidUtils.Share();
    }
}
