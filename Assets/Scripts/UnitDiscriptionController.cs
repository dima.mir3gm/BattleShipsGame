﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class UnitDiscriptionController : MonoBehaviour, OnSummaryLevelUpped, OnHitPointChanged, OnNicknameChanged {

    [SerializeField] private Text level;
    [SerializeField] private Text playerName;
    [SerializeField] private GameObject HitPoints;
    [SerializeField] private LevelSystem levelSystem;
    [SerializeField] private Unit unit;

    void Start()
    {
        if(levelSystem == null || unit == null)
            return;
        levelSystem.AddSummaryLevelEventHandler(this);
        unit.AddHpChangedListener(this);
        unit.NicknameListener = this;
        playerName.text = unit.Nickname;
    }

    public void Init(LevelSystem ls, Unit unit)
    {
        levelSystem = ls;
        this.unit = unit;

        levelSystem.AddSummaryLevelEventHandler(this);
        unit.AddHpChangedListener(this);
        unit.NicknameListener = this;
        playerName.text = unit.Nickname;
    }

    void Update()
    {
        Transform tf = unit.transform;
        transform.position = new Vector3(tf.position.x, tf.position.y, 0);
    }

    public void SummaryLevelUpped(int current_level)
    {
        level.text = current_level.ToString();
    }

    public void OnHitPointChanged(HitPoints hp)
    {
        HitPoints.GetComponent<UpgradeStatusLoadController>().SetPercentValue( (hp.CurrentHp/hp.MaxHp) * 100 );
    }


    public void OnNicknameChanged(string nickname)
    {
        playerName.text = nickname;
    }
}
