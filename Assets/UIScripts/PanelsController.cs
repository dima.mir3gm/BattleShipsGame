﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(Canvas))]
public class PanelsController : MonoBehaviour {

    private List<GameObject> panles;

    [SerializeField] private ErrorControlPanels errorControl;
    private GameObject errorPanel;

    void Start(){
        panles = new List<GameObject>(transform.childCount);
        for (int i = 0; i < transform.childCount; i++)
        {
            panles.Add(transform.GetChild(i).gameObject);
        }
        errorPanel = errorControl.gameObject;
    }


    public void OnPanel(int index)
    {
        ChangePanelActive(index, true);
        hideAllErrorPanels();
    }

    public void OffPanel(int index)
    {
        ChangePanelActive(index, false);
    }

    public void errorPanelOff()
    {
        errorPanel.SetActive(false);
    }

    public void errorPanelOn()
    {
        errorPanel.SetActive(true);
    }

    public void ChangePanelActive(int index, bool state)
    {
        //if (panles.Count >= index)
        //{
         //   return;
        //}

        panles[index].SetActive(state);
    }

    public void showErorPanel()
    {
        Debug.Log("Show error panel");
        //errorControl.gameObject.SetActive(true);
        errorPanelOn();
        errorControl.showErrorPanel();
    }

    public void showServerBusyPanel()
    {
        errorPanelOn();
        errorControl.showServerBusyPanel();
    }

    public void hideAllErrorPanels()
    {
        if (errorControl.gameObject.activeSelf)
        {
            errorControl.hideAllPanels();
            errorControl.gameObject.SetActive(false);
        }
    }

}
