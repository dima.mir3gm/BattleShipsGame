﻿using UnityEngine;
using System.Collections;
using AppodealAds.Unity.Api;
using CnControls;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour, OnUnitDie
{

    [SerializeField] private Unit unit;
    private bool left_down;
    private bool right_down;

    private bool left_turned;
    private bool right_turned;

	// Use this for initialization
	void Start ()
	{
	    unit.ListenerDie = this;
	}

#if UNITY_STANDALONE
    // Update is called once per frame
    void Update () {

        if (Input.GetKeyDown(KeyCode.A))
	    {
            if(Input.GetKey(KeyCode.D))
                unit.Rotate(Unit.RotateTypes.Cancel);
            else
	            left_down = true;
            
	    }
	    else
	    {
	        left_down = false;
	    }

	    if(Input.GetKeyDown(KeyCode.D))
	    {
            if (Input.GetKey(KeyCode.A))
                unit.Rotate(Unit.RotateTypes.Cancel);
            else
                right_down = true;
	    }
	    else
	    {
	        right_down = false;
	    }

	    if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
	    {
	        if (Input.GetKey(KeyCode.A))
	            left_down = true;
            else if (Input.GetKey(KeyCode.D))
                right_down = true;
            else
	            unit.Rotate(Unit.RotateTypes.Cancel);
	    }

        if(Input.GetKey(KeyCode.W))
            unit.Faster();
        else if(Input.GetKey(KeyCode.S))
            unit.Slowly();

        if(right_down && !left_down)
            unit.Rotate(Unit.RotateTypes.Right);
        else if(left_down && !right_down)
            unit.Rotate(Unit.RotateTypes.Left);

	    if (Input.GetKey(KeyCode.Mouse0))
	    {
	        unit.Attack();
	    }
	}
#elif UNITY_ANDROID

    void Update () {

        float hor = CnInputManager.GetAxis("Horizontal");
        float ver = CnInputManager.GetAxis("Vertical");


        bool A = false;
        bool D = false;
        bool S = false;
        bool W = false;

        if (hor > 0)
            D = true;
        else if(hor < 0)
            A = true;

        if (ver > 0)
            W = true;
        else if(ver < 0)
            S = true;

	    if (A)
	    {
            if(D)
                unit.Rotate(Unit.RotateTypes.Cancel);
            else
	            left_down = true;
            
	    }
	    else
	    {
	        left_down = false;
	    }

	    if(D)
	    {
            if (A)
                unit.Rotate(Unit.RotateTypes.Cancel);
            else
                right_down = true;
	    }
	    else
	    {
	        right_down = false;
	    }

	    if (A || D)
	    {
	        if (A)
	            left_down = true;
            else if (D)
                right_down = true;
            
	    }
        else
            unit.Rotate(Unit.RotateTypes.Cancel);

        if (W)
            unit.Faster();
        else if(S)
            unit.Slowly();

        if(right_down && !left_down)
            unit.Rotate(Unit.RotateTypes.Right);
        else if(left_down && !right_down)
            unit.Rotate(Unit.RotateTypes.Left);

	    if (CnInputManager.GetButton("Jump"))
	    {
	        unit.Attack();
	    }
	}

#endif

    public void OnUnitDie(Unit unit)
    {
        Destroy(unit.gameObject);
        Appodeal.show(Appodeal.INTERSTITIAL | Appodeal.NON_SKIPPABLE_VIDEO);
        SceneManager.LoadScene("start");
    }
}
