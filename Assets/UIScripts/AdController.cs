﻿using System.Collections;
using System.Collections.Generic;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine;

public class AdController : MonoBehaviour
{
	[SerializeField]
	private string appKey;
	
	private void Awake()
	{
		Appodeal.setTesting(true);
		Appodeal.confirm(Appodeal.SKIPPABLE_VIDEO);
		Appodeal.initialize(appKey, Appodeal.BANNER + Appodeal.REWARDED_VIDEO + Appodeal.INTERSTITIAL + Appodeal.SKIPPABLE_VIDEO);
	}

	public void ShowBar()
	{
		Appodeal.show(Appodeal.BANNER_TOP);
	}

	public void StopBar()
	{
		Appodeal.hide(Appodeal.BANNER_TOP);
	}

	public void ShowVideo()
	{
		Appodeal.show(Appodeal.SKIPPABLE_VIDEO);
	}

	public void ShowInterstitial()
	{
		Appodeal.show(Appodeal.INTERSTITIAL);
	}

	public void ShowRewardVideo()
	{
		Appodeal.show(Appodeal.REWARDED_VIDEO);
	}

	public bool isRewardVideo()
	{
		return Appodeal.isLoaded(Appodeal.REWARDED_VIDEO) || Appodeal.isPrecache(Appodeal.REWARDED_VIDEO);
	}

	public void AddListener(IRewardedVideoAdListener listener)
	{
		Appodeal.setRewardedVideoCallbacks(listener);
	}
}

