﻿using UnityEngine;
using System.Collections;

public class LevelPanelListener : MonoBehaviour, OnLevelChanged
{

    [SerializeField] private LevelSystem levelSystem;

    public LevelSystem LevelSystem
    {
        get { return levelSystem; }
    }

    void Awake()
    {
        levelSystem.AddLevelEventHandler(this);
    }
        
    public void HP_click()
    {
        levelSystem.UpgradeAmmoLvl(LevelSystem.AMMO_HP);
    }

    public void HP_REGENERATION_click()
    {
        levelSystem.UpgradeAmmoLvl(LevelSystem.AMMO_HP_REGEN);
    }

    public void CANNON_RANGE_click()
    {
        levelSystem.UpgradeAmmoLvl(LevelSystem.AMMO_CANON_RANGE);
    }

    public void CANNON_DAMAGE_click()
    {
        levelSystem.UpgradeAmmoLvl(LevelSystem.AMMO_CANON_DAMAGE);
    }

    public void CANNON_RELOAD_SPD_click()
    {
        levelSystem.UpgradeAmmoLvl(LevelSystem.AMMO_RELOAD_SPEED);
    }

    public void MOVMENT_SPD_click()
    {
        levelSystem.UpgradeAmmoLvl(LevelSystem.AMMO_MOVMENT_SPEED);
    }

    public void TURN_SPD_click()
    {
        levelSystem.UpgradeAmmoLvl(LevelSystem.AMMO_TURN_SPEED);
    }

    public void RAM_DMG_click()
    {
        levelSystem.UpgradeAmmoLvl(LevelSystem.AMMO_RAM_DAMAGE);
    }

    public void LevelChanged(string key, int lvl)
    {
        LevelLoadController contr = GetComponent<LevelLoadController>();
        switch (key)
        {
            case LevelSystem.AMMO_HP:
                contr.ChangeLoad(0, lvl);
                break;

            case LevelSystem.AMMO_HP_REGEN:
                contr.ChangeLoad(1, lvl);
                break;

            case LevelSystem.AMMO_CANON_RANGE:
                contr.ChangeLoad(2, lvl);
                break;

            case LevelSystem.AMMO_CANON_DAMAGE:
                contr.ChangeLoad(3, lvl);
                break;

            case LevelSystem.AMMO_RELOAD_SPEED:
                contr.ChangeLoad(4, lvl);
                break;

            case LevelSystem.AMMO_MOVMENT_SPEED:
                contr.ChangeLoad(5, lvl);
                break;

            case LevelSystem.AMMO_TURN_SPEED:
                contr.ChangeLoad(6, lvl);
                break;

            case LevelSystem.AMMO_RAM_DAMAGE:
                contr.ChangeLoad(7, lvl);
                break;
        }
    }
}
