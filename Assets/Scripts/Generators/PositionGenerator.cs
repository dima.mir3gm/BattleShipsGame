﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PositionGenerator
{
    public const float GENERATED_OBJ_DIST = 20;

    private static readonly PositionGenerator positionGenerator = new PositionGenerator();
    private static bool inited = false;

    private List<GameObject> objectList;
    private readonly System.Random random;


    private float min_x;
    private float min_y;
    private float max_x;
    private float max_y;


    private PositionGenerator()
    {
        random = new System.Random();
    }


    public static void initGenerator(List<GameObject> objectsList, Vector2 top_left_map, Vector2 bottom_right_map)
    {
        positionGenerator.objectList = objectsList;
        inited = true;

        positionGenerator.min_x = top_left_map.x;
        positionGenerator.max_x = bottom_right_map.x;

        positionGenerator.min_y = bottom_right_map.y;
        positionGenerator.max_y = top_left_map.y;
    }

    public void AddObjects(List<GameObject> list)
    {
        objectList.AddRange(list);
    }

    public void AddObject(GameObject obj)
    {
        objectList.Add(obj);
    }

    public static PositionGenerator getGenerator()
    {
        if(inited)
            return positionGenerator;
        throw new Exception("Init generator please");
    }


    public Vector3 Next()
    {
        int lf = (int)(min_x*10);
        int hf = (int)(max_x*10);
        float x = (float) random.Next(lf, hf)/10;

        lf = (int)(min_y * 10);
        hf = (int)(max_y * 10);
        float y = (float)random.Next(lf, hf) / 10;
        Vector3 vec3 = new Vector3(x, y);
        bool much = true;

        foreach (var obj in objectList)
        {
            if(Vector3.Distance(vec3, obj.transform.position) < GENERATED_OBJ_DIST)
            {
                much = false;
                break;
            }
        }

        if (much)
            return vec3;
        else
            return Next();
    }
}
