﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody2D))]
public abstract class Unit : MonoBehaviour , OnLevelChanged, OnHitPointChanged, OnScoreChanged
{

    private static List<Unit> units = new List<Unit>();
    private static OnScoreChangedScoreboard scoreboard;

    [SerializeField] protected Weapon weapon_side;
    [SerializeField] protected Weapon weapon_top;
    [SerializeField] protected Weapon weapon_back;

    [SerializeField] protected string nickname;
    [SerializeField] protected bool isGodMode;
    [SerializeField] protected float hp;
    [SerializeField] protected float hp_regenereation;
    [SerializeField] protected float cannon_range;
    [SerializeField] protected float cannon_damage;
    [SerializeField] protected float reload_speed;
    [SerializeField] protected float movment_speed;
    [SerializeField] protected float turn_speed;
    [SerializeField] protected float ram_damage;

    [SerializeField] private float autorepair_tick_time = 5;
    [SerializeField] private OnUnitDie listener;

    private Dictionary<int, Unit> boardList;  
    private OnGemDestructed gem_destructed_litener;
    private OnNicknameChanged panel_listener;
    private float autorepair_timer;
    protected HitPoints hit_points;
    private Unit last_attacker;

    private float based_hp;
    private float based_hp_regeneration;
    private float based_cannon_range;
    private float based_cannon_damage;
    private float based_reload_speed;
    private float based_movment_speed;
    private float based_turn_speed;
    private float based_ram_damage;


    private float after_die_time = 1f;
    private float after_die_timer = 0;
    private bool die = false;
    protected bool changed = false;

    [SerializeField] protected LevelSystem level_system;

    [SerializeField] protected bool block_move;

    public Unit()
    {
        units.Add(this);
    }


    public bool IsDie
    {
        get { return die; }
    }

    public OnNicknameChanged NicknameListener
    {
        set { panel_listener = value; }
    }

    public OnGemDestructed GemDestructedListener
    {
        set { gem_destructed_litener = value; }
    }

    public static void SetScoreboardListener(OnScoreChangedScoreboard scb)
    {
        scoreboard = scb;
    }

    public string Nickname
    {
        set
        {
            nickname = value;
            if(panel_listener != null)
                panel_listener.OnNicknameChanged(nickname);
        }
        get { return nickname; }
    }

    public bool IsGodMode
    {
        set
        {
            isGodMode = value;
            if(isGodMode) CannonDamage = float.MaxValue;
        }
        get { return isGodMode; }
    }

    public OnUnitDie ListenerDie
    {
        set { listener = value; }
        get { return listener; }
    }

    public bool BlockMove
    {
        set { block_move = value; }
        get { return block_move; }
    }

    protected Vector3 old_pos;

    public Vector3 Old_pos
    {
        get { return old_pos; }
    }

    public float HP
    {
        set { hp = value; }
        get { return hp; }
    }

    public float HpRegenereation
    {
        get { return hp_regenereation; }
        set { hp_regenereation = value; }
    }

    public float CannonRange
    {
        get { return cannon_range; }
        set { cannon_range = value; }
    }

    public float CannonDamage
    {
        get { return cannon_damage; }
        set
        {
            if (isGodMode) cannon_damage = float.MaxValue;
            else cannon_damage = value;
        }
    }

    public float ReloadSpeed
    {
        get { return reload_speed; }
        set { reload_speed = value; }
    }

    public float MovmentSpeed
    {
        get { return movment_speed; }
        set { movment_speed = value; }
    }

    public float TurnSpeed
    {
        get { return turn_speed; }
        set { turn_speed = value; }
    }

    public float RamDamage
    {
        get { return ram_damage; }
        set { ram_damage = value; }
    }

    public void Hit(Unit unit, float damage)
    {
        last_attacker = unit;
        if(!isGodMode) hit_points.Damage(damage);
    }

    public abstract void Attack();

    public abstract void Move();

    public abstract void Upgrade(UpgradeType type, Weapon weapon);

    public abstract void Faster();

    public abstract void Slowly();

    public abstract float GetCurrentVelocity();

    public abstract float GetCurrentSpeedCoef();
    

    public abstract void Rotate(RotateTypes type);

    public enum RotateTypes
    {
        Right,
        Left,
        Cancel
    }

    void FixedUpdate()
    {
        Move();
    }

    void Update()
    {
        if (autorepair_timer >= autorepair_tick_time)
        {
            autorepair_timer = 0;
            hit_points.Heal(hp_regenereation);
        }
        else
        {
            autorepair_timer += Time.deltaTime;
        }
    }

    void Start()
    {
        setWeaponsPerf();
        changed = true;
    }

    void Awake()
    {
        hit_points = new HitPoints();
        hit_points.MaxHp = hp;
        hit_points.CurrentHp = hp;
        based_hp = hp;
        based_hp_regeneration = hp_regenereation;
        based_cannon_range = cannon_range;
        based_cannon_damage = cannon_damage;
        based_reload_speed = reload_speed;
        based_movment_speed = movment_speed;
        based_turn_speed = turn_speed;
        based_ram_damage = ram_damage;
        hit_points.AddListener(this);
        level_system.AddLevelEventHandler(this);
        level_system.AddScoreHandler(this);
    }

    protected void setWeaponsPerf()
    {
        Weapon[] weapon = new Weapon[3];
        weapon[0] = weapon_back;
        weapon[1] = weapon_side;
        weapon[2] = weapon_top;

        for (int i = 0; i < 3; i++)
        {
            if (weapon[i] != null)
            {
                weapon[i].SetPerfomance(cannon_damage, reload_speed, cannon_range);
                //weapon[i].Damage = cannon_damage;
                //weapon[i].ReloadSpeed = reload_speed;
                //weapon[i].Range = cannon_range;
            }
        }
    }

    public void OnBoardCollision(Unit unit)
    {
        Hit(unit, unit.ram_damage);
    }

    public void AddExpiriance(float exp)
    {
        level_system.Expiriance = exp;
    }

    public void GemDestructed(GameObject gem)
    {
        if(gem_destructed_litener != null)
            gem_destructed_litener.OnGemDestructed(gem);
    }

    private void CheckOnLive()
    {
        if (die)
        {
            if (after_die_timer < after_die_time)
            {
                after_die_timer += Time.deltaTime;
            }
            else
            {
                die = false;
                after_die_timer = 0;
            }
        }
    }


    public void LevelChanged(string key, int lvl)
    {
        switch (key)
        {
            case LevelSystem.AMMO_HP:
                hp = based_hp + BonusLevelStructure.HP_BONUS[lvl]*based_hp / 100;
                break;

            case LevelSystem.AMMO_HP_REGEN:
                hp_regenereation = based_hp_regeneration + based_hp_regeneration*BonusLevelStructure.HP_REGEN_BONUS[lvl] / 100;
                break;

            case LevelSystem.AMMO_CANON_RANGE:
                cannon_range = based_cannon_range + based_cannon_range*BonusLevelStructure.CANON_RANGE_BONUS[lvl] / 100;
                break;

            case LevelSystem.AMMO_CANON_DAMAGE:
                cannon_damage = based_cannon_damage + based_cannon_damage*BonusLevelStructure.CANON_DAMAGE_BONUS[lvl] / 100;
                break;

            case LevelSystem.AMMO_RELOAD_SPEED:
                reload_speed = based_reload_speed + based_reload_speed*BonusLevelStructure.RELOAD_SPEED_BONUS[lvl] / 100;
                break;

            case LevelSystem.AMMO_MOVMENT_SPEED:
                movment_speed = based_movment_speed + based_movment_speed*BonusLevelStructure.MOVMENT_SPEED_BONUS[lvl] / 100;
                break;

            case LevelSystem.AMMO_TURN_SPEED:
                turn_speed = based_turn_speed + based_turn_speed*BonusLevelStructure.TURN_SPEED_BONUS[lvl] / 100;
                break;

            case LevelSystem.AMMO_RAM_DAMAGE:
                ram_damage = based_ram_damage + based_ram_damage*BonusLevelStructure.RAM_DAMAGE_BONUS[lvl] / 100;
                break;
        }
        setWeaponsPerf();
    }

    public void AddHpChangedListener(OnHitPointChanged lis)
    {
        hit_points.AddListener(lis);
    }

    public void RemoveHpChangedListener(OnHitPointChanged lis)
    {
        hit_points.RemoveListener(lis);
    }

    public void OnHitPointChanged(HitPoints hp)
    {
        if (hp.CurrentHp <= 0)
        {
            die = true;
            last_attacker.AddExpiriance(level_system.CurrentRaitingPoints/2);
            if(listener != null)
                listener.OnUnitDie(this);
        }
    }

    public void Recover()
    {
        hit_points.CurrentHp = hit_points.MaxHp;
    }


    public void OnScoreChanged(float score)
    {
        if(scoreboard != null)
            scoreboard.OnScoreChanged(score, this);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        CkeckBoardDamage(col);
    }

    void OnCollisionStay2D(Collision2D col)
    {
        CkeckBoardDamage(col);
    }

    void OnCollisionExit2D(Collision2D col)
    {
        CkeckBoardDamage(col);
    }

    private void CkeckBoardDamage(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Enemy"))
        {
            col.gameObject.GetComponent<Unit>().OnBoardCollision(this);
        }
    }
}

public interface OnUnitDie
{
    void OnUnitDie(Unit unit);
}

public interface OnScoreChangedScoreboard
{
    void OnScoreChanged(float score, Unit unit);
}

public interface OnNicknameChanged
{
    void OnNicknameChanged(string nickname);
}

public interface OnGemDestructed
{
    void OnGemDestructed(GameObject gem);
}
