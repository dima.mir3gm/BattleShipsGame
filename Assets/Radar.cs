﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Radar : MonoBehaviour
{

    [SerializeField] private TargetFollower follower;
    private List<IOnTargetFounded> target_listener;

    void Awake()
    {
        target_listener = new List<IOnTargetFounded>();
    }

    public void AddTargetFinderListener(IOnTargetFounded listener)
    {
        target_listener.Add(listener);
    }

    public bool RemoveTargetFinderListener(IOnTargetFounded listener)
    {
        return target_listener.Remove(listener);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("COLLISION");
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        GetTarget(col);
    }

    void OnTriggerStay2D(Collider2D col)
    {
        GetTarget(col);
    }

    void OnTriggerExit2D(Collider2D col)
    {
        GetTarget(col);
    }

    public void GetTarget(Collider2D col)
    {
        if (col.tag.Equals("Gem"))
        {
            if (follower != null)
            {
                follower.SetTarget(col.gameObject, TargetFollower.TargetType.Gem);
            }
        }
        if (col.tag.Equals("Enemy"))
        {
            if (follower != null)
            {
                follower.SetTarget(col.gameObject, TargetFollower.TargetType.Enemy);
            }
            NotifyListeners(col);
        }
    }

    private void NotifyListeners(Collider2D col)
    {
        foreach (var listener in target_listener)
        {
            listener.OnTargetFounded(col.gameObject, Vector3.Distance(transform.position, col.transform.position));
        }
    }
}

public interface IOnTargetFounded
{
    void OnTargetFounded(GameObject target, float dist);
}
