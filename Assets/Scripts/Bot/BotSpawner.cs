﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class BotSpawner : MonoBehaviour, OnUnitDie
{
    private List<GameObject> spawnedBots;


    [SerializeField] private DynamicPanelCreator panleCreator;
    [SerializeField] private PositionManager manager;
    /*
    [SerializeField] private List<Unit> bots;
    */
    [SerializeField] private int max_bots = 10;
    [SerializeField] private BotController bot_prefab;
    [SerializeField] private int max_bot_start_exp = 3000;
    [SerializeField] private float delayed_create = 3;

    private List<string> side_weap_list;
    private List<string> rear_weap_list;
    private List<string> deck_weap_list;

    private List<GameObject> toCreate; 

    private bool st = true;

    private System.Random rand;

    void Start()
    {
        
        spawnedBots = new List<GameObject>();
        rand = new System.Random();

        /*
        foreach (var bot in bots)
        {
            bot.ListenerDie = this;
            bot.Nickname = NamePool.Nickname_pool[rand.Next(0, NamePool.Nickname_pool.Length - 1)];
        }
        */

        side_weap_list = new List<string>();
        foreach (var side_weap in UpdateStructure.SIDE_UPDATE)
        {
            side_weap_list.AddRange(UpdateStructure.UPDATE_TREE[side_weap]);
        }

        rear_weap_list = new List<string>();
        foreach (var rear_weap in UpdateStructure.REAR_UPDATE)
        {
            rear_weap_list.AddRange(UpdateStructure.UPDATE_TREE[rear_weap]);
        }

        deck_weap_list = new List<string>();
        foreach (var deck_weap in UpdateStructure.DECK_UPDATE)
        {
            deck_weap_list.AddRange(UpdateStructure.UPDATE_TREE[deck_weap]);
        }

        
    }

    void Update()
    {
        if (st)
        {
            if (delayed_create > 0)
            {
                delayed_create -= Time.deltaTime;
            }
            else
            {
                CreateBotStage1();
            }
        }
        else if(toCreate.Count > 0)
        {
            CreateBotStage2();
        }
    } 

    public void OnUnitDie(Unit unit)
    {
        unit.Recover();
        unit.gameObject.transform.position = manager.GetPosition();
    }

    public void CreateBotStage1()
    {
        st = false;
        toCreate = new List<GameObject>(max_bots);
        for (int i = 0; i < max_bots; i++)
        {
            toCreate.Add(Instantiate(bot_prefab.gameObject, manager.GetPosition(), new Quaternion(), transform) as GameObject);
        }
    }

    public void CreateBotStage2()
    {
        foreach (var bot in toCreate)
        {
            BotController contr = bot.GetComponent<BotController>();
            Unit unit = bot.GetComponentInChildren<Unit>();
            panleCreator.CreateUnitPanel(bot.GetComponent<LevelSystem>(), unit);

            contr.SetStartExp(rand.Next(max_bot_start_exp));
            contr.SetSummaryLvl();

            Weapon wp = null;
            UpgradeType[] types = { UpgradeType.SIDE, UpgradeType.DECK, UpgradeType.REAR, };
            List<string>[] list = {side_weap_list, deck_weap_list, rear_weap_list};

            for (int i = 0; i < types.Length; i++)
            {
                try
                {
                    wp = (Instantiate(Resources.Load(list[i][rand.Next(list[i].Count - 1)])) as GameObject).GetComponent<Weapon>();

                    if (wp != null)
                        unit.Upgrade(types[i], wp);
                }
                catch (Exception ex)
                { }
            }


            unit.Nickname = NamePool.Nickname_pool[rand.Next(0, NamePool.Nickname_pool.Length - 1)];
            unit.ListenerDie = this;

            manager.AddObject(bot);
            spawnedBots.Add(bot);
        }

        toCreate.Clear();
    }

    
}
