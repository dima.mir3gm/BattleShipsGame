﻿using UnityEngine;
using System.Collections;
using System;

public class SimpleBoat : Unit
{

    [SerializeField] private float byTimeCoef = 1;
    [SerializeField] private float velocity_powering = 1;
    [SerializeField] private float max_speed_coef = 1.5f;
    [SerializeField] private float min_speed_coef = 0.5f;
    [SerializeField] private float current_speed_coef = 1;

    private float old_angle;
    

    public override void Attack()
    {
        if(weapon_back != null)
            weapon_back.Attack();
        if(weapon_side != null)
            weapon_side.Attack();
        if(weapon_top != null)
            weapon_top.Attack();
    }

    public override void Move()
    {
        CheckAngle();
        if(!changed || block_move)
            return;
        
        old_pos = transform.position;
        old_angle = transform.eulerAngles.z;
        float angle = ((transform.eulerAngles.z + 90) * Mathf.PI) / 180;
        GetComponent<Rigidbody2D>().velocity = new Vector2(current_speed_coef * velocity_powering * movment_speed * Mathf.Cos(angle),current_speed_coef * velocity_powering * movment_speed * Mathf.Sin(angle));
    }

    private void CheckAngle()
    {
        if (old_angle != transform.eulerAngles.z)
            changed = true;
    }

    public override void Upgrade(UpgradeType type, Weapon weapon)
    {
        switch (type)
        {
            case UpgradeType.DECK:
                if (weapon_top != null)
                    Destroy(weapon_top.gameObject);
                weapon_top = weapon;
                weapon_top.transform.parent = transform;
                weapon_top.transform.localPosition = weapon_top.transform.position;
                weapon_top.transform.localEulerAngles = weapon_top.transform.eulerAngles;
                break;

            case UpgradeType.REAR:
                if(weapon_back != null)
                    Destroy(weapon_back.gameObject);
                weapon_back = weapon;
                weapon_back.transform.parent = transform;
                weapon_back.transform.localPosition = weapon_back.transform.position;
                weapon_back.transform.localEulerAngles = weapon_back.transform.eulerAngles;
                break;

            case UpgradeType.SIDE:
                if (weapon_side != null)
                    Destroy(weapon_side.gameObject);
                weapon_side = weapon;
                weapon_side.transform.parent = transform;
                weapon_side.transform.localPosition = weapon_side.transform.position;
                weapon_side.transform.localEulerAngles = weapon_side.transform.eulerAngles;
                break;
        }
        setWeaponsPerf();
    }

    public override void Faster()
    {
        if(current_speed_coef >= max_speed_coef)
            return;

        current_speed_coef += 0.5f*Time.deltaTime;
        if (current_speed_coef > max_speed_coef)
            current_speed_coef = max_speed_coef;

        changed = true;
    }

    public override float GetCurrentSpeedCoef()
    {
        return current_speed_coef;
    }

    public override void Slowly()
    {
        if (current_speed_coef <= min_speed_coef)
            return;

        current_speed_coef -= 0.5f * Time.deltaTime;
        if (current_speed_coef < min_speed_coef)
            current_speed_coef = min_speed_coef;

        changed = true;
    }

    public override float GetCurrentVelocity()
    {
        return movment_speed*current_speed_coef*velocity_powering;
    }

    public override void Rotate(RotateTypes type)
    {
        switch (type)
        {
            case RotateTypes.Left:
            {
                GetComponent<Rigidbody2D>().angularVelocity = turn_speed;
                break;
            }

            case RotateTypes.Right:
            {
                GetComponent<Rigidbody2D>().angularVelocity = -turn_speed;
                break;
            }

            case RotateTypes.Cancel:
            {
                GetComponent<Rigidbody2D>().angularVelocity = 0;
                break;
            }

            default:
                break;
        }
    }

}
