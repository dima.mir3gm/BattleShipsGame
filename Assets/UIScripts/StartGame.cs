﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour {

    public void StartGameScene()
    {
        SceneManager.LoadScene("game");
    }
}
