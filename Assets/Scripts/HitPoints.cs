﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class HitPoints
{

    private List<OnHitPointChanged> observers; 

    private float max_hp;
    private float current_hp;

    public HitPoints()
    {
        observers = new List<OnHitPointChanged>();
    }

    public float MaxHp
    {
        get { return max_hp; }
        set
        {
            float coef = current_hp/max_hp;
            max_hp = value;
            current_hp = max_hp*coef;
            notify();
        }
    }

    public float CurrentHp
    {
        set
        {
            current_hp = value;
            notify();
        }
        get { return current_hp; }
    }

    public void Damage(float damage)
    {
        current_hp -= damage;
        notify();
    }

    public void Heal(float hp)
    {
        if(current_hp >= max_hp)
            return;
        else if (current_hp + hp >= max_hp)
        {
            current_hp = max_hp;
        }
        else if(current_hp > 0)
        {
            current_hp += hp;
        }
        else
        {
            return;
        }
        notify();
    }

    public float GetCurrentHpInPercent()
    {
        return (current_hp/max_hp)*100;
    }

    public void AddListener(OnHitPointChanged listener)
    {
        observers.Add(listener);
    }

    public void RemoveListener(OnHitPointChanged listener)
    {
        observers.Remove(listener);
    }

    private void notify()
    {
        foreach (var listener in observers)
        {
            listener.OnHitPointChanged(this);
        }
    }
}

public interface OnHitPointChanged
{
    void OnHitPointChanged(HitPoints hp);
}
