﻿using UnityEngine;
using System.Collections;

public class LevelStructure : MonoBehaviour
{

    private static float[] levelStructure =
    {
        0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100
    };

    private static float[] upgradeStructures =
    {
        0, 50, 750, 1500, 3000, 10000, 25000, 100000
    };

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public static float[] GetLevelStructure()
    {
        return levelStructure;
    }

    public static float[] GetUpgradeStructure()
    {
        return upgradeStructures;
    }
}
