﻿using UnityEngine;
using System.Collections;

public class DynamicPanelCreator : MonoBehaviour
{

    [SerializeField] private GameObject prefab;

    public void CreateUnitPanel(LevelSystem ls, Unit unit)
    {
        GameObject go = Instantiate(prefab, transform) as GameObject;
        go.GetComponent<UnitDiscriptionController>().Init(ls, unit);
    }
}
