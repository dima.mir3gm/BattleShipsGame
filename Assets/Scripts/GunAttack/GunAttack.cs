﻿using UnityEngine;
using System.Collections;

public class GunAttack : MonoBehaviour, IGunAttack
{
    
    [SerializeField] protected float velocity_powering = 10;
    [SerializeField] private Vector3 START_VECTOR = new Vector3(0, 15, 0);
    [SerializeField] private bool angleCorrected = false;
    private Unit unit;
    protected BulletPool pool;

    protected virtual void Start()
    {
        pool = GameObject.Find("BulletPool").GetComponent<BulletPool>();

        Transform t = transform;
        while (unit == null)
        {
            unit = t.GetComponent<Unit>();
            t = t.parent;
        }
    }

    public virtual void Fire(float damage, float range)
    {
        if(pool == null)
            return;

        GameObject bullet = pool.InstantiateBullet();
        bullet.transform.parent = transform;
        bullet.transform.localPosition = START_VECTOR;
        float angle;
        if (!angleCorrected)
            angle = ((transform.eulerAngles.z) *Mathf.PI) / 180;
        else
        {
            angle = ((transform.eulerAngles.z + 90) * Mathf.PI) / 180;
        }
        bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity_powering * Mathf.Cos(angle), velocity_powering * Mathf.Sin(angle));
        bullet.transform.parent = null;
        bullet.transform.localScale = transform.localScale;
        Bullet bul = bullet.GetComponent<Bullet>();
        bul.BulletLife = range;
        bul.Parent = unit;
        bul.Damage = damage;
    }
}
