﻿using UnityEngine;
using System.Collections;
using System.Text;


[RequireComponent(typeof(LevelSystem))]
public class BotController : MonoBehaviour
{

    [SerializeField] private Unit unit;
    [SerializeField] private float checkLvlUpTime = 10;
    [SerializeField] private TargetFollower follower;

    private float current_lvlup_timer;
    private ActionType current_action = ActionType.NoAction;
    private bool block;
    private float block_timer;
    private float action_timer;
    private LevelSystem levelSystem;
    private float current_timer = 0;



    public void init()
    {
        if (levelSystem == null)
        {
            levelSystem = GetComponent<LevelSystem>();
        }
        
    }

	// Use this for initialization
	void Start ()
	{
        init();
    }
	
	// Update is called once per frame
	void Update () {
	    if (CheckPosition())
	    {
	    }
	    else
	    {
	        if (follower != null && !block)
	        {
                
	            switch (follower.MoveToTarget())
	            {
                    case TargetFollower.RotateTypes1.Left:
                        current_action = ActionType.InverseAngleLeft;
	                    action_timer = 10f;
                        break;

                    case TargetFollower.RotateTypes1.Right:
                        current_action = ActionType.InverseAngleRight;
                        action_timer = 10f;
                        break;

                    case TargetFollower.RotateTypes1.Cancel:
	                    current_action = ActionType.NoAction;
                        action_timer = 10f;
                        break;

                    case TargetFollower.RotateTypes1.NoAction:
                        break;
	            }
	        }
	    }
        Action();
        Timing();
        TryLvlUp();
    }

 



    private bool CheckPosition()
    {
        if(block)
            return false;

        float x = unit.transform.position.x;
        float y = unit.transform.position.y;
        if (x > 4000 && y > 4000)
        {
            if (x > y)
            {
                current_action = ActionType.InverseAngleRight;
            }
            else
            {
                current_action = ActionType.InverseAngleLeft;
            }
        }
        else if (x > 4000 && y < -4000)
        {
            if (x > Mathf.Abs(y))
            {
                current_action = ActionType.InverseAngleLeft;
            }
            else
            {
                current_action = ActionType.InverseAngleRight;
            }
        }
        else if(x < -4000 && y > 4000)
        {
            if (Mathf.Abs(x) > y)
            {
                current_action = ActionType.InverseAngleLeft;
            }
            else
            {
                current_action = ActionType.InverseAngleRight;
            }
        }
        else if(x < -4000 && y < -4000)
        {
            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                current_action = ActionType.InverseAngleRight;
            }
            else
            {
                current_action = ActionType.InverseAngleLeft;
            }
        }
        else if(x > 4000)
        {
            current_action = ActionType.InverseAngleRight;
        }
        else if(x < -4000)
        {
            current_action = ActionType.InverseAngleRight;
        }
        else if(y > 4000)
        {
            current_action = ActionType.InverseAngleRight;
        }
        else if(y < -4000)
        {
            current_action = ActionType.InverseAngleRight;
        }
        else
        {
            return false;
        }
        block = true;
        block_timer = 1;
        action_timer = 0.6f;
        return true;
    }

    private void Action()
    {
        switch (current_action)
        {
            case ActionType.InverseAngleRight:
                unit.Rotate(Unit.RotateTypes.Right);
                break;

            case ActionType.InverseAngleLeft:
                unit.Rotate(Unit.RotateTypes.Left);
                break;

            case ActionType.NoAction:
                unit.Rotate(Unit.RotateTypes.Cancel);
                break;

            default:
                unit.Rotate(Unit.RotateTypes.Cancel);
                break;
        }
        unit.Attack();
    }

    private void Timing()
    {
        if (block_timer > 0)
        {
            block_timer -= Time.deltaTime;
        }
        else
        {
            block = false;
        }

        if (action_timer > 0)
        {
            action_timer -= Time.deltaTime;
        }
        else
        {
            current_action = ActionType.NoAction;
        }
    }

    private void TryLvlUp()
    {
        if (current_lvlup_timer >= checkLvlUpTime)
        {
            current_lvlup_timer = 0;
            SetSummaryLvl();
        }
        else
        {
            current_lvlup_timer += Time.deltaTime;
        }
    }

    public void SetStartExp(float exp)
    {
        levelSystem.Expiriance = exp;
    }

    public void SetSummaryLvl()
    {
        string[] keys = levelSystem.ammo_keys;
        bool[] states = new bool[keys.Length];
        while (true)
        {
            int mask = 0;
            for (int i = 0; i < keys.Length; i++)
            {
                states[i] = levelSystem.UpgradeAmmoLvl(keys[i]);
                if (states[i])
                    mask++;
            }

            
            if(mask == 0)
                return;

        }
    }

    

    
}

public enum ActionType
{
    InverseAngleLeft,
    InverseAngleRight,
    NoAction
}
