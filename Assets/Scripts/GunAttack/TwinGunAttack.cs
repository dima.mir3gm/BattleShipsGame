﻿using UnityEngine;
using System.Collections;

public class TwinGunAttack : MonoBehaviour ,IGunAttack {

    public void Fire(float damage, float range)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).GetComponent<IGunAttack>().Fire(damage, range);
        }
    }
}
