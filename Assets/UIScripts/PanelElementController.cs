﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PanelElementController : MonoBehaviour
{
    [SerializeField] private RectTransform element1;
    [SerializeField] private RectTransform element2;
    [SerializeField] private RectTransform element3;
    [SerializeField] private RectTransform element4;

    private bool state = false;

    private float[] x_poses =
    {
        5,
        55,
        105,
        155
    };

    private List<RectTransform> elementList;

    private const int MAX_CAPACITY = 4;

    public void AddElement(RectTransform element)
    {
        if(elementList.Count >= MAX_CAPACITY - 1)
            return;

        
        element.SetParent(transform);
        element.pivot = Vector2.zero;
        element.anchorMax = Vector2.zero;
        element.anchorMin = Vector2.zero;
        element.localPosition = new Vector3(-element.rect.size.x/2, x_poses[elementList.Count], 0);
        elementList.Add(element);
    }

    public void RemoveElement(RectTransform element)
    {
        if(elementList.Remove(element))
        {
            element.position = new Vector3(-10000, 0 ,0);
        }
    }

    public void RemoveElement(int i)
    {
        elementList[i].position = new Vector3(-10000, 0, 0);
        elementList.Remove(elementList[i]);
    }

	// Use this for initialization
	void Start () {
	    elementList = new List<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void OnClick()
    {
        state = !state;
        if(state)
            ActivateList();
        else
            DeactivateList();
    }

    public void ActivateList()
    {
        if(element1 != null)
            AddElement(element1);
        if (element2 != null)
            AddElement(element2);
        if (element3 != null)
            AddElement(element3);
        if (element4 != null)
            AddElement(element4);
    }

    public void DeactivateList()
    {
        if (element1 != null)
            RemoveElement(element1);
        if (element2 != null)
            RemoveElement(element2);
        if (element3 != null)
            RemoveElement(element3);
        if (element4 != null)
            RemoveElement(element4);
    }

    public void Clear()
    {
        List<RectTransform> pc = new List<RectTransform>(elementList);
        foreach (var element in pc)
        {
            RemoveElement(element);
        }

        pc.Clear();
    }
}
